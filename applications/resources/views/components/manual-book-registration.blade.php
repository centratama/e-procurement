@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-primary"></h1>
            <div class="panel panel-default">
                <div class="panel-heading">Panduan Pengguna - eProcurement</div>

                <div class="panel-body">
                    <img class="img-responsive" src="{{ url('/') }}/assets/panduan.jpg" height="600px">
                </div>
            </div>
        </div><!--end .col -->
    </div>
@endsection
