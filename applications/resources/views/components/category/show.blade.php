@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>
					Rincian Kategori &raquo; {{$data['category'][0]->category_name}}
				</header>
			</div>
			<div class="card-body floating-label">
				<div class="row">
					<div class="col-md-2">
						<b>Nama Kategori</b>:
					</div>
					<div class="col-md-4">
						{{$data['category'][0]->category_name}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Catatan</b>:
					</div>
					<div class="col-md-4">
						{{$data['category'][0]->remarks}}
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection