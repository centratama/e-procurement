@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
	{{ Form::model($data['category'], array('route' => ['update-category', $data['category'][0]->id], 'method' => 'patch', 'class'=>'form')) }}
		{{ csrf_field() }}
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Kategori &raquo; {{$data['category'][0]->category_name}}</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<div class="form-group{{ $errors->has('category_name') ? ' has-error' : '' }}">
						<input type="text" name="category_name" class="form-control" id="category_name" value="{{$data['category'][0]->category_name}}">
						<label for="category_name">
							{!! required('Nama Kategori') !!}
						</label>
						@if ($errors->has('category_name'))
							<span class="help-block">
								<strong>{{ $errors->first('category_name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
						<input type="text" name="remarks" class="form-control" id="remarks" value="{{$data['category'][0]->remarks}}">
						<label for="remarks">
							Catatan
						</label>
						@if ($errors->has('remarks'))
							<span class="help-block">
								<strong>{{ $errors->first('remarks') }}</strong>
							</span>
						@endif
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="reset" class="btn btn-danger ink-reaction">Reset</button>
					<button type="submit" class="btn btn-primary ink-reaction">Kirim</button>
				</div>
			</div>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection