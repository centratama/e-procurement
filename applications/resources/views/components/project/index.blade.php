@extends('layouts.app')
@section('content')
<?php
$no = 0;
?>
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Daftar Projek</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<center>
						<span>
							@if (Auth::user()->user_id != 'BOD')
								<a href="{{url('project/create')}}" class="btn btn-info ink-reaction"><i class="fa fa-plus"></i> Tambah</a>
							@endif
						</span>
					</center><br>
					<span class="divider"></span>
					<table class="table table-responsive">
						<thead>
							<tr>
								<th>#</th>
								<th>Perusahaan</th>
								<th>Nama Projek</th>
								<th>Deskripsi</th>
								<th>Kategori Pekerjaan</th>
								<th>Status</th>
								<th>Tools</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data['project'] as $key => $project)
							<tr>
								<th>{{++$no}}.</th>
								<td>{{$project->company_name}}</td>
								<td>{{$project->project_name}}</td>
								<td>{{$project->desc}}</td>
								<td>{!! $data['category'][$key] !!}</td>
								<td>{!! statusProject($project->status, $project->status_review) !!}</td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn ink-reaction btn-primary dropdown-toggle" data-toggle="dropdown">
											Action <i class="fa fa-caret-down"></i>
										</button>
										<ul class="dropdown-menu animation-expand" role="menu">
											@if(Auth::user()->user_id != "BOD")
												<li><button onclick="editProject({{$project->id}});" class="btn btn-block btn-flat btn-default-dark ink-reaction" {!! isDisabledButton($project->status, $project->status_review)!!}>Edit</button></li>
												@if (Auth::user()->user_role == "SUPER ADMIN")
													<li><button onclick="publishProject({{$project->id}});" class="btn btn-block btn-flat btn-default-dark ink-reaction" {!! isDisabledButtonByReview($project->status, $project->status_review)!!}>Publish</button></li>
												@endif
											@endif
											<li><button onclick="viewProject({{$project->id}});" class="btn btn-block btn-flat btn-default-dark ink-reaction">View Detail</button></li>
											</ul>
										</div><!--end .btn-group -->
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					
				</div>
			</div>
		</div>
	</div>
</div>

<!-- script -->
<script type="text/javascript">
	function publishProject(id)
	{
		window.location.href="{{url('project/publish/')}}/"+id;
	}

	function viewProject(id)
	{
		window.location.href="{{url('project/view/')}}/"+id;
	}
	function editProject(id)
	{
		window.location.href="{{url('project/edit/')}}/"+id;
	}	
</script>
@endsection