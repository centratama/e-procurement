@extends('layouts.app')
@section('content')
@if($data['project'][0]->status == 1)
<!-- <meta http-equiv="refresh" content="7" id="refresh-content"> -->
@endif
<style type="text/css">
	#clockdiv{
    	font-family: sans-serif;
    	color: #fff;
    	display: inline-block;
    	font-weight: 100;
    	text-align: center;
    	font-size: 30px;
	}

	#clockdiv > div{
    	padding: 10px;
    	border-radius: 3px;
    	background: #00BF96;
    	display: inline-block;
	}

	#clockdiv div > span{
    	padding: 15px;
    	border-radius: 3px;
    	background: #00816A;
    	display: inline-block;
	}

	.smalltext{
    	padding-top: 5px;
    	font-size: 16px;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Rincian Projek &raquo; {{$data['project'][0]->project_name}} {!! statusProject($data['project'][0]->status, $data['project'][0]->status_review) !!}</header>
				<input type="hidden" name="project_id" id="project_id" value="{{$data['project'][0]->id}}">
			</div>
			<div class="card-body floating-label">
				<div class="row">
					<div class="col-md-2">
						<b>Nama Projek</b>:
					</div>
					<div class="col-md-4">
						{{$data['project'][0]->project_name}}
					</div>
					<div class="col-md-2">
						<b>Diminta Oleh</b>:
					</div>
					<div class="col-md-4">
						{{$data['project'][0]->company_name}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Jadwal Projek</b>:
					</div>
					<div class="col-md-4">
						{{date("d M, Y", strtotime($data['project'][0]->project_start_date))}} - {{date("d M, Y", strtotime($data['project'][0]->project_end_date))}}
					</div>
					<div class="col-md-2">
						<b>Jadwal Penawaran</b>:
					</div>
					<div class="col-md-4">
						{{date("d M, Y h:i A", strtotime($data['project'][0]->biding_start))}} - {{date("d M, Y h:i A", strtotime($data['project'][0]->biding_finish))}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Site</b>:
					</div>
					<div class="col-md-4">
						@if ($data['project'][0]->site != null)
							{{$data['project'][0]->site}}
						@else
							<i class="md md-remove-circle-outline text-danger"></i>
						@endif
						
					</div>
					@if (Auth::user()->user_role == "SUPER ADMIN")
					<div class="col-md-2">
						<b>Rentang Harga</b>:
					</div>
					<div class="col-md-4">
						{{number_format($data['project'][0]->price_lower_limit, 2, ',', '.')}} - {{number_format($data['project'][0]->price_upper_limit, 2, ',', '.')}}
					</div>
					@else 
					<div class="col-md-2">
						<b>Harga Maksimum</b>:
					</div>
					<div class="col-md-4">
						{{-- {{number_format($data['project'][0]->price_lower_limit, 2, ',', '.')}} --}}{{number_format($data['project'][0]->price_upper_limit, 2, ',', '.')}}
					</div>
					@endif
				</div>
				<div class="row">
					{{-- <div class="col-md-2">
						<b>Komponen Harga</b>:
					</div>
					<div class="col-md-4">
						@if (empty($data['price_comp']))
							<i class="md md-remove-circle-outline text-danger"></i>
						@else
							<ul>
							@foreach ($data['price_comp'] as $key => $component)
								<li>{{$component->component}}</li>
							@endforeach
							</ul>
						@endif
						
					</div> --}}
					<div class="col-md-2">
						<b>Jenis Projek</b>:
					</div>
					<div class="col-md-4">
						{{ $data['project_type']->desc }}
					</div>
					<div class="col-md-2">
						<b>Kelipatan Harga</b>:
					</div>
					<div class="col-md-4">
						{{number_format($data['project'][0]->multiples_value, 2, ',', '.')}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Deskripsi Projek</b>:
					</div>
					<div class="col-md-10">
						{{$data['project'][0]->desc}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Deskripsi Daerah Projek</b>:
					</div>
					<div class="col-md-10">
						{{$data['project'][0]->desc_place}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Dokumen Pendukung</b>:
					</div>
					<div class="col-md-10">
						{!! $attach !!}
					</div>
				</div>
				@if(Auth::user()->user_vendor != NULL && !empty($data['pv']))
				<br>
				<div class="row" style="font-size: 16px;">
					<div class="col-md-12">
						@if(in_array($data['project'][0]->status, array('1','4')))
							@if (!empty($data['current_bid']))
								<center>
									<b>Penawaran Terakhir {{Auth::user()->name}} adalah</b>
									{{number_format($data['current_bid']->prices_bid, 2, ',', '.')}}<br>
									<b>Peringkat</b>: {{$data['current_bid']->rank}} dari {{count($data['count_pv'])}} Peserta
								</center>
							@endif
						@elseif($data['project'][0]->status == 4)
						<!-- <center>
							Vendor yang dipilih adalah <b>{{$data['winner'][0]->vendor_name}}</b> dengan nominal
							<b>{{number_format($data['winner'][0]->prices_bid, 2, ',', '.')}}</b> <label class="label label-success">Close Complete!</label>
						</center> -->
						@endif
					</div>
				</div>
				@elseif(Auth::user()->user_vendor == NULL && !empty($data['best_price']))
				<br>
				<div class="row" style="font-size: 16px;">
					<div class="col-md-12">
						@if(in_array($data['project'][0]->status, array('1','4')))
						<center>
							Penawaran Terbaik Saat Ini dari <b>{{$data['best_price'][0]->vendor_name}}</b> dengan nominal
							<b>{{number_format($data['best_price'][0]->prices_bid, 2, ',', '.')}}</b>
						</center>
						@elseif($data['project'][0]->status == 4)
						<center>
							Vendor yang dipilih adalah <b>{{$data['winner'][0]->vendor_name}}</b> dengan nominal
							<b>{{number_format($data['winner'][0]->prices_bid, 2, ',', '.')}}</b> <label class="label label-success">Close Complete!</label>
						</center>
						@endif
					</div>
				</div>
				@endif

				@if (in_array($data['project'][0]->status, array('0','1')))
				<div class="row">
					<div class="col-md-12">
						<center>
							<div id="clockdiv">
							<h2><font color="black" id="label-bid">BID ENDS IN</font></h2>
  								<div>
    								<span class="days" id="days"></span>
    								<div class="smalltext">Days</div>
  								</div>
  								<div>
    								<span class="hours" id="hours"></span>
    								<div class="smalltext">Hours</div>
  								</div>
  								<div>
    								<span class="minutes" id="minutes"></span>
    								<div class="smalltext">Minutes</div>
  								</div>
  								<div>
    								<span class="seconds" id="seconds"></span>
    								<div class="smalltext">Seconds</div>
  								</div>
							</div>
						</center>
					</div>
				</div>
				@endif
				<br>
				<br>
				@if ($data['project'][0]->status == 0)
				@if (Auth::user()->user_vendor != NULL && !empty($data['pv']))
					@if (in_array($data['pv'][0]->is_upload, array(0,2)))
						<div class="card panel">
							<div class="card-head card-head-xs collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
								<header>Unggah Dokumen</header>
								<div class="tools">
									<a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
								</div>
							</div>
							<div id="accordion2-1" class="collapse in">
							{{ Form::open(array('url'=>'project-vendor/upload', 'class'=>'form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
								<div class="card-body">
									<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
										<input type="file" name="file" class="form-control">
										@if ($errors->has('file'))
											<span class="help-block">
												<strong>{{ $errors->first('file') }}</strong>
											</span>
										@endif
									</div>
									<input type="hidden" name="id" value="{{$data['pv'][0]->id}}">
								</div>
								<div class="card-actionbar">
									<div class="card-actionbar-row">
										<button type="submit" class="btn btn-primary ink-reaction">Kirim</button>
									</div>
								</div>
							{{ Form::close() }}
							</div>
						</div><!--end .panel -->
					@endif
				@endif
				@endif
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					@if(Auth::user()->user_vendor != NULL && !empty($data['pv']) && $data['project'][0]->status == 1)
						@if($data['pv'][0]->status == 0)
						{{-- <button class="btn btn-danger ink-reaction" onclick="reject({{$data['project'][0]->id}})">Reject</button>
						<button class="btn btn-primary ink-reaction" onclick="accept({{$data['project'][0]->id}})"">Accept</button> --}}
						@elseif($data['pv'][0]->status == 1)
						<button onclick="removeMeta()" class="btn btn-success ink-reaction" data-toggle="modal" data-target="#bid-form" id="btn-bid" @if(!empty($data['current_bid']) && $data['current_bid']->status ==1) disabled @endif><i class="md md-assignment"></i> Place a New Bid</button>
						@endif
					@elseif(Auth::user()->user_vendor == NULL)
						@if($data['project'][0]->status == 1)
							<button onclick="removeMeta()" class="btn btn-info ink-reaction" data-toggle="modal" data-target="#close-form">Cancel Project</button>
						@elseif($data['project'][0]->status == NULL)
							@if(Auth::user()->user_role == "SUPER ADMIN")
								@if(Auth::user()->user_id == "eproc2")
									@if ($data['project'][0]->status_review === NULL || $data['project'][0]->status_review === 2)
										@if ($data['project_type']->validation_type == 1)
										<button class="btn btn-primary ink-reaction" data-toggle="modal" data-target="#edit-price-range">Edit Price Range</button>
										@endif
										<button class="btn btn-warning ink-reaction" data-toggle="modal" data-target="#add-price-comp">Add Price Component</button>

										@if (!empty($data['price_comp']))
											<button class="btn btn-info ink-reaction" onclick="needReview({{$data['project'][0]->id}})">Need Review BOD</button>
										@endif
									@endif
								@elseif(Auth::user()->user_id == "BOD")
									@if ($data['project'][0]->status_review === 0)
										<button class="btn btn-info ink-reaction" data-toggle="modal" data-target="#accept-or-revise-form">Accept or Revise</button>
									@endif
								@endif
							@endif
						@endif
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@if(Auth::user()->user_vendor != NULL && !empty($data['pv']))
	@if($data['pv'][0]->status == 1)
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Riwayat Penawaran</header>
			</div>
			<div class="card-body floating-label">
				<div class="row">
					<div class="col-sm-6">
						<i class="glyphicon glyphicon-stop text-success"></i> Harga Terendah
						<br>
						<i class="glyphicon glyphicon-stop text-danger"></i> Bukan Harga Terendah	
					</div>
					<div class="col-sm-6">
						@if ($data['project'][0]->status == 1)
						<font style="float: right;">
							@if ($data['history'] == NULL)
								{{$data['max_bid']}} kesempatan melakukan <i>Bidding</i>
							@else
								Sisa kesempatan: {{$data['max_bid'] - count($data['history'])}}
							@endif
						</font>
						@endif
					</div>
				</div>
				<table class="table table-responsive">
					<thead>
						<th>#</th>
						<th>Harga (Rp.)</th>
						<th>Tanggal</th>
						<th></th>
						<th style="visibility: hidden;"></th>
					</thead>
					<tbody>
					<?php
						$no = 0;
					?>
						@if ($data['history'] == null)
						<tr>
							<td colspan="7">Data is not available.</td>
						</tr>
						@else
						@foreach($data['history'] as $history)
						<tr>
							<th>{{++$no}}.</th>
							<td class="{!! statusBids($history->status) !!}">{{number_format($history->prices_bid, 2, ',', '.')}}</td>
							<td>{{date("d M, Y h:i A", strtotime($history->created_at))}}</td>
							<td>
							@if($no == 1)
								current bid
							@else
								expired
							@endif
							</td>
							<td style="visibility: hidden;">
								@if ($history->status == 1 && $data['project'][0]->status == 1)
									<button class="btn btn-primary ink-reaction" data-toggle="modal" data-target="#revise-form-{{$history->id}}" onclick="removeMeta()" id="btn-revise">Revise</button>
								@endif
							</td>
						</tr>
						@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- FORM MODAL FOR REVISE BID PRICE -->
@if($data['history'] != null)
@foreach($data['history'] as $history)
@if ($history->status == 1)
<div class="modal fade" id="revise-form-{{$history->id}}" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="formModalLabel">Revise Bid</h4>
				<h6>(Maximum Total Price is {{number_format($data['project'][0]->price_upper_limit, 2, ',', '.')}})</h6>
			</div>
			<form class="form-horizontal" role="form">
				<div class="modal-body">
					@foreach($data['price_comp'] as $key => $component)
						<div class="form-group">
							<div class="col-sm-3">
								<label for="email1" class="control-label">{{$component->component}}</label>
							</div>
							<div class="col-sm-9">
								<input type="text" name="price_comp[{{$component->id}}]" id="price_comp-{{$component->id}}" class="form-control price" placeholder="Harga" onkeypress="return isNumberKey(event)" value="{{explode(",", $history->prices_comp)[$key]}}">
							</div>
						</div>
					@endforeach
					<div class="form-group">
						<div class="col-sm-3">
							<label for="email1" class="control-label">Harga (Rp)</label>
						</div>
						<div class="col-sm-6">
							<input type="text" name="price" id="price" class="form-control" placeholder="Harga" onkeypress="return isNumberKey(event)" value="{{$history->prices_bid}}" onchange="this.value = minmax(this.value, {{$data['project'][0]->price_lower_limit}}, {{$data['project'][0]->price_upper_limit}})" readonly>
							<input type="hidden" name="id" id="id" value="{{$history->id}}">
							<input type="hidden" name="project_id" id="project_id" value="{{$data['project'][0]->id}}">
							<input type="hidden" name="current_bid" id="current_bid" value="{{$history->prices_bid}}">
						</div>
						<div class="col-sm-3">
							<button class="btn btn-success" id="btn-calculate">Calculate</button>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button id="btn-submit-revise" type="button" class="btn btn-primary" disabled>Submit</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->
@endif
@endforeach
@endif

<!-- BEGIN FORM MODAL MARKUP PLACE A NEW BID -->
<div class="modal fade" id="bid-form" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="formModalLabel">Place a New Bid </h4>
				<h6>(Maximum Total Price is {{number_format($data['project'][0]->price_upper_limit, 2, ',', '.')}})</h6>
			</div>
			<form class="form-horizontal" role="form">
				<div class="modal-body">
					@foreach($data['price_comp'] as $key => $component)
						<div class="form-group">
							<div class="col-sm-3">
								<label for="email1" class="control-label">{{$component->component}}</label>
							</div>
							<div class="col-sm-9">
								<input type="text" name="price_comp[{{$component->id}}]" id="price_comp-{{$component->id}}" class="form-control price" placeholder="Harga" onkeypress="return isNumberKey(event)">
							</div>
						</div>
					@endforeach
					<div class="form-group">
						<div class="col-sm-3">
							<label for="email1" class="control-label">Harga (Rp)</label>
						</div>
						<div class="col-sm-6">
							<input type="text" name="price" id="price" class="form-control" placeholder="Harga" onkeypress="return isNumberKey(event)" onchange="this.value = minmax(this.value, {{$data['project'][0]->price_lower_limit}}, {{$data['project'][0]->price_upper_limit}})" readonly>
							<input type="hidden" name="tpv_id" id="tpv_id" value="{{$data['pv'][0]->id}}">
							<input type="hidden" name="project_id" id="project_id" value="{{$data['project'][0]->id}}">
							@if($data['current_bid'] == NULL)
								<input type="hidden" name="last_bid" id="last_bid" value="">
							@else
								<input type="hidden" name="last_bid" id="last_bid" value="{{$data['current_bid']->prices_bid}}">
							@endif
						</div>
						<div class="col-sm-3">
							<button class="btn btn-success" id="btn-calculate">Calculate</button>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button id="btn-submit-bid" type="button" class="btn btn-primary" disabled>Submit</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->
	@endif
@elseif(Auth::user()->user_vendor == NULL)
	@if($data['project'][0]->status == 2)
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-head style-primary">
						<header>Daftar Penawar</header>
					</div>
					<div class="card-body tab-content">
						<table class="table table-responsive">
							<thead>
								<tr>
									<th>#</th>
									<th>Vendor</th>
									<th>Last bid</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 0;
								?>

								@foreach($data['selected_vendor'] as $vendor)
								<tr>
									<th>{{++$no}}.</th>
									<td><button class="btn btn-link" data-toggle="modal" data-target="#history-{{$vendor['vendor']->tpv_id}}">{{$vendor['vendor']->vendor_name}}</button></td>
									<td>
										@if (!empty($vendor['price'][0]))
											{{number_format($vendor['price'][0]->prices_bid, 2, ',', '.')}}
										@else
											Tidak Memberikan Penawaran.
										@endif
									</td>
									<td>
										<button class="btn btn-primary ink-reaction" value="{{$vendor['vendor']->tpv_id}}" onclick="chooseVendor(this.value)" @if (empty($vendor['price'][0])) disabled @endif>Pilih</button>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		@foreach($data['history'] as $history)
			@if(!empty($history))
				<div class="modal fade" id="history-{{$history[0]->tpv_id}}" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="formModalLabel">Riwayat Penawaran dari {{$history[0]->vendor_name}}</h4>
							</div>
							<form class="form-horizontal" role="form">
								<div class="modal-body">
									<div class="form-group">
										<div class="col-sm-12">
											<table class="table table-responsive">
												<thead>
													<tr>
														<th>#</th>
														<th>Harga (Rp.)</th>
														<th>Tanggal</th>
													</tr>
												</thead>
												<tbody>
													<?php
														$no = 0;
													?>
													@foreach($history as $price)
													<tr>
														<th>{{++$no}}.</th>
														<td>{{number_format($price->prices_bid, 2, ',', '.')}}</td>
														<td>{{date("d M, Y h:i A", strtotime($price->created_at))}}</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			<!-- END FORM MODAL MARKUP -->
			@endif
		@endforeach
	@elseif(in_array($data['project'][0]->status, array("1","3","4")))
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-head">
						<ul class="nav nav-tabs" data-toggle="tabs">
							<li class="active"><a href="#first">#</a></li>
						@foreach($data['history'] as $history)
							@if(!empty($history))
							<li><a href="#VND-{{$history[0]->tpv_id}}">{{$history[0]->vendor_name}}</a></li>
							@endif
						@endforeach
						</ul>
					</div>
					<div class="card-body tab-content">
						<div class="tab-pane active" id="first">
							@if (!empty($data['best_price']))
							<center>
								Penawaran Terbaik Saat Ini dari <b>{{$data['best_price'][0]->vendor_name}}</b> dengan nominal
								<b>{{number_format($data['best_price'][0]->prices_bid, 2, ',', '.')}}</b>
							</center>
							@else
							<center>
								Belum tersedianya penawaran terbaik.
							</center>
							@endif
						</div>
						@foreach($data['history'] as $history)
							@if(!empty($history))
								<div class="tab-pane" id="VND-{{$history[0]->tpv_id}}">
									<header>Riwayat Penawara {{$history[0]->vendor_name}}</header>
									<table class="table table-responsive">
										<thead>
											<tr>
												<th>#</th>
												<th>Harga</th>
												<th>Tanggal</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$no = 0;
										?>
										@foreach($history as $price)
											<tr>
												<th>{{++$no}}.</th>
												<td>{{number_format($price->prices_bid, 2, ',', '.')}}</td>
												<td>{{date("d M, Y h:i A", strtotime($price->created_at))}}</td>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
							@endif
						@endforeach
					</div><!--end .card-body -->
				</div>
			</div>
		</div>
	@endif
	@if(!empty($data['pv']) && $data['project'][0]->status == 0)
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-head style-primary">
						<header>Dafta Peserta</header>
					</div>
					<div class="card-body floating-label">
						<table class="table table-responsive">
							<thead>
								<tr>
									<th>#</th>
									<th>Vendor</th>
									<th>Status</th>
									<th>Tenggat waktu</th>
									<th>Terakhir diubah</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
								@if(empty($data['pv']))
								<tr>
									<td colspan="4">Data belum tersedia.</td>
								</tr>
								@else
									@foreach($data['pv'] as $key => $pv)
									<tr>
										<th>{{++$key}}</th>
										<td>{{$pv->vendor_name}}</td>
										<td>
											@if($pv->is_upload == 0)
												<?php 
													$label = "label-danger";
													$text = "Belum Mengunggah";
												?>
											@elseif($pv->is_upload == 1)
												<?php 
													$label = "label-success";
													$text = "Sudah Mengunggah";
													if ($pv->status == 1) {
														$text = "Sudah Tervalidasi";
													} elseif ($pv->status == 2) {
														$label = "label-danger";
														$text = "Tidak Ikut Serta";
													}
												?>
											@else
												<?php 
													$label = "label-warning";
													$text = "Revisi";
												?>
											@endif
											<span class="label {{$label}}">{{$text}}</span>
										</td>
										<td>{{$pv->invitation_due_date}}</td>
										<td>{{$pv->updated_at}}</td>
										<td>
											<div class="btn-group">
												<button type="button" class="btn ink-reaction btn-primary dropdown-toggle" data-toggle="dropdown">
													Action <i class="fa fa-caret-down"></i>
												</button>
												<ul class="dropdown-menu animation-expand" role="menu">
													<li><button class="btn btn-block btn-flat btn-default-dark ink-reaction" data-toggle="modal" data-target="#view-attach-{{$pv->tpv_id}}">View Result</button></li>
												</ul>
											</div><!--end .btn-group -->
										</td>
									</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div><!--end .card-body -->
				</div>
			</div>
		</div>
		<!-- FORM MODAL FOR VIEW ATTACHMET -->
		@foreach($data['pv'] as $key => $pv)
		<div class="modal fade" id="view-attach-{{$pv->tpv_id}}" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="formModalLabel">{{$pv->vendor_name}}</h4>
					</div>
					<form class="form-horizontal" role="form">
						<div class="modal-body">
							@if($pv->file != null)
							<embed src="{{url('/')}}/attachments/document/agreement/{{$pv->file}}" width="100%" height="600px" class="pdf-viewer"></embed>
							@endif
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-block" {!! $data['pv_disabled'][$key] !!} onclick="reviseVendor({{$pv->tpv_id}});">Revise</button>
							<button type="button" class="btn btn-primary btn-block" {!! $data['pv_disabled'][$key] !!} onclick="acceptVendor({{$pv->tpv_id}});">Accept</button>
						</div>
					</form>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		@endforeach
		<!-- END FORM MODAL MARKUP -->
	@endif
@endif

@if(Auth::user()->user_vendor == NULL)
<!-- FORM MODAL FOR ClOSE PROJECT -->
<div class="modal fade" id="close-form" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="formModalLabel">Close Project</h4>
			</div>
			<form class="form-horizontal" role="form">
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-3">
							<label for="email1" class="control-label">Close/Cancel Project</label>
						</div>
						<div class="col-sm-9">
							<select name="close_type" class="form-control" id="close_type" disabled>
								<option value="2">Cancel Project</option>
								{{-- @foreach($data['close_type'] as $type) --}}
								{{-- @endforeach --}}
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label for="email1" class="control-label">Reason</label>
						</div>
						<div class="col-sm-9">
							<textarea class="form-control" id="reason" name="reason"></textarea>
							<input type="hidden" name="project_id" id="project_id" value="{{$data['project'][0]->id}}">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button id="btn-submit-close-project" type="button" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->

<!-- BEGIN FORM MODAL MARKUP ACCEPT OR REVISE PROJECT -->
<div class="modal fade" id="accept-or-revise-form" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="formModalLabel">Accept or Revise Project</h4>
				<h6>({{$data['project'][0]->project_name}})</h6>
			</div>
			<form class="form-horizontal" role="form">
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form form-control" name="review_desc" id="review_desc" onkeyup="removeAttrDisabled()"></textarea>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button id="btn-accept-project" type="button" class="btn btn-primary btn-block" onclick="acceptOrRevise({{$data['project'][0]->id}}, 1)" disabled>Accept</button>
					<button id="btn-revise-project" type="button" class="btn btn-default btn-block" onclick="acceptOrRevise({{$data['project'][0]->id}}, 2)" disabled>Revise</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->

<div class="modal fade" id="add-price-comp" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="formModalLabel">Add Price Component</h4>
			</div>
			{{ Form::open(array('url'=>'project/store-price-comp', 'class'=>'form-horizontal', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-3">
							<label for="component" class="control-label">Komponen</label>
						</div>
						<div class="col-sm-9">
							<input type="text" name="component" class="form-control" id="component" value="{{ old('component') }}" placeholder="Ex: Jasa, Barang">
							<input type="hidden" name="project_id" value="{{ $data['project'][0]->id }}">
							<input type="hidden" name="validation_type" value="{{ $data['project_type']->validation_type }}">
						</div>
					</div>

					@if ($data['project_type']->validation_type == 2)
						<div class="form-group">
							<div class="col-sm-3">
								<label for="lower_limit_price" class="control-label">Harga Batas Bawah</label>
							</div>
							<div class="col-sm-9">
								<input type="text" name="lower_limit_price" class="form-control" id="lower_limit_price" value="{{ old('lower_limit_price') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label for="upper_limit_price" class="control-label">Harga Batas Atas</label>
							</div>
							<div class="col-sm-9">
								<input type="text" name="upper_limit_price" class="form-control" id="upper_limit_price" value="{{ old('upper_limit_price') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label for="multiples_price" class="control-label">Kelipatan Harga</label>
							</div>
							<div class="col-sm-9">
								<input type="text" name="multiples_price" class="form-control" id="multiples_price" value="{{ old('multiples_price') }}">
							</div>
						</div>
					@endif

					<div class="form-group">
						<div class="col-sm-3">
							<label for="unit" class="control-label">Unit</label>
						</div>
						<div class="col-sm-9">
							<input type="text" name="unit" class="form-control" id="unit" value="{{ old('unit') }}" placeholder="Satuan Unit">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label for="quantity" class="control-label">Kuantitas</label>
						</div>
						<div class="col-sm-9">
							<input type="text" name="quantity" class="form-control" id="quantity" value="{{ old('quantity') }}">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="reset" name="reset" class="btn btn-danger" value="Reset">
					<input type="submit" name="submit" class="btn btn-primary" value="Submit">
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="edit-price-range" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="formModalLabel">Edit Price Range</h4>
			</div>
			{{ Form::open(array('url'=>'project/update-price-range', 'class'=>'form-horizontal', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-3">
							<label for="price_lower_limit" class="control-label">Harga Batas Bawah</label>
						</div>
						<div class="col-sm-9">
							<input type="text" name="price_lower_limit" class="form-control" id="price_lower_limit" value="{{ $data['project'][0]->price_lower_limit }}">
							<input type="hidden" name="project_id" value="{{ $data['project'][0]->id }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label for="price_upper_limit" class="control-label">Harga Batas Atas</label>
						</div>
						<div class="col-sm-9">
							<input type="text" name="price_upper_limit" class="form-control" id="price_upper_limit" value="{{ $data['project'][0]->price_upper_limit }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label for="multiples_value" class="control-label">Kelipatan Harga</label>
						</div>
						<div class="col-sm-9">
							<input type="text" name="multiples_value" class="form-control" id="multiples_value" value="{{ $data['project'][0]->multiples_value }}">
						</div>
					</div>					
				</div>
				<div class="modal-footer">
					<input type="reset" name="reset" class="btn btn-danger" value="Reset">
					<input type="submit" name="submit" class="btn btn-primary" value="Submit">
				</div>
			</form>
		</div>
	</div>
</div>

@endif
<!-- Script action button -->
<script type="text/javascript">
	@if (Auth::user()->user_vendor != NULL)
		function reject(id) {
			window.location.href="{{url('project-vendor/reject/')}}/"+id;
		}

		function accept(id) {
	 		window.location.href="{{url('project-vendor/accept/')}}/"+id;
	 	}

	 	function placeBid(id) {
	 		window.location.href="{{url('project-vendor/placeBid?id=')}}/"+id;
	 	}

	 	function isNumberKey(evt)
      	{
        	var charCode = (evt.which) ? evt.which : event.keyCode
        	if (charCode > 31 && (charCode < 48 || charCode > 57))
        	return false;

        	return true;
      	}

      	$(".price").on('keyup', function(){
    		var n = parseInt($(this).val().replace(/\D/g,''),10);
    		$(this).val(n.toLocaleString());
		});

      	// FOR PLACE A NEW BID
      	function minmax(value, min, max) 
		{
			// $('#btn-submit-bid').prop('disabled', true);
			$('#btn-submit-revise').prop('disabled', true);
    		if(parseInt(value) < min || isNaN(parseInt(value))) {
    			alert("The Minimum Price is "+min);
        		return min; 
    		} else if (parseInt(value) > max) {
    			alert("The Maximun Price is "+max);
        		return max; 
    		} else {
    			// $('#btn-submit-bid').prop('disabled', false);
    			$('#btn-submit-revise').prop('disabled', false);
    			return value;
    		}
		}

      	$(document).ready(function(){
      		$('#btn-submit-bid').click(function(e){
			// $("#form-problem").submit(function(){
				e.preventDefault();

				var fd = new FormData();
				var price = "";
				fd.append('prices_bid', ($("#price").val()));
				fd.append('tpv_id', ($("#tpv_id").val()));
				fd.append('project_id', ($("#project_id").val()));
				fd.append('last_bid', ($("#last_bid").val()));
				var i = 0;
				@foreach($data['price_comp'] as $component)
					price = $('#price_comp-{{$component->id}}').val();
					price = price.toString();
					price = price.replace(/\D/g,"");
					fd.append('prices_comp['+i+']' , parseInt(price));
					++i;
				@endforeach

				var url = "{{url('/')}}/project-bids/save";

				$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
				});
			
				$.ajax({
					url: url,
					type: "POST",
					data: fd,
					async: false,
					dataType: 'json',
					cache: false,
					contentType: false,
                	processData: false,
					success: function(data) {
						window.location.href = "{{url('/')}}/show-project?id={{$crypt}}";
					}
				});
			});

			$('#btn-calculate').click(function(e){
				e.preventDefault();
				var total = 0;
				var price = "";
				@foreach($data['price_comp'] as $component)
					price = $('#price_comp-{{$component->id}}').val();
					price = price.toString();
					price = price.replace(/\D/g,"");
					total += parseInt(price);
				@endforeach
				 if (isNaN(total)) {
				 	$('#btn-submit-bid').prop('disabled', true);
				 	$('#btn-submit-revise').prop('disabled', true);
				 } else {
				 	$('#price').val(commaSeparateNumber(total));
					$('#btn-submit-bid').prop('disabled', false);
					$('#btn-submit-revise').prop('disabled', false);
				 }
			});
      	});

      	function commaSeparateNumber(val){
    		while (/(\d+)(\d{3})/.test(val.toString())){
      			val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    		}
    		return val;
  		}

      	$(document).ready(function(){
      		$('#btn-submit-revise').click(function(e){
			// $("#form-problem").submit(function(){
				e.preventDefault();

				var fd = new FormData();
				var price = "";
				fd.append('prices_bid', ($("#price").val()));
				fd.append('id', ($("#id").val()));
				fd.append('project_id', ($("#project_id").val()));
				fd.append('current_bid', ($("#current_bid").val()));

				var i = 0;
				@foreach($data['price_comp'] as $component)
					price = $('#price_comp-{{$component->id}}').val();
					price = price.toString();
					price = price.replace(/\D/g,"");
					fd.append('prices_comp['+i+']' , parseInt(price));
					++i;
				@endforeach

				var url = "{{url('/')}}/project-bids/revise";

				$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
				});
			
				$.ajax({
					url: url,
					type: "POST",
					data: fd,
					async: false,
					dataType: 'json',
					cache: false,
					contentType: false,
                	processData: false,
					success: function(data) {
						window.location.href = "{{url('/')}}/show-project?id={{$crypt}}";
					}
				});
			});
      	});
	 @else
	 $(document).ready(function(){
      		$('#btn-submit-close-project').click(function(e){
			// $("#form-problem").submit(function(){
				e.preventDefault();

				var fd = new FormData();
				fd.append('close_type', ($("#close_type").val()));
				fd.append('reason', ($("#reason").val()));
				fd.append('project_id', ($("#project_id").val()));

				var url = "{{url('/')}}/project/close";

				$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
				});
			
				$.ajax({
					url: url,
					type: "POST",
					data: fd,
					async: false,
					dataType: 'json',
					cache: false,
					contentType: false,
                	processData: false,
					success: function(data) {
						window.location.href = "{{url('/')}}/project/view/"+$("#project_id").val();
					}
				});
			});
      	});


	 	function acceptVendor(id)
	 	{
	 		var url = "{{url('/')}}/project-vendor/confirm";
	 		var fd = new FormData();
	 		var redirect = "{{url('/')}}/project/view/"+{{$data['project'][0]->id}};
	 		fd.append('id', id);
	 		fd.append('status', 1);

	 		ajaxPost(fd, url, redirect);
	 	}

	 	function reviseVendor(id)
	 	{
	 		var url = "{{url('/')}}/project-vendor/confirm";
	 		var fd = new FormData();
	 		var redirect = "{{url('/')}}/project/view/"+{{$data['project'][0]->id}};
	 		fd.append('id', id);
	 		fd.append('is_upload', 2);

	 		ajaxPost(fd, url, redirect);
	 	}

	 	function ajaxPost(fd, url, redirect)
	 	{
	 		$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
			});
			
			$.ajax({
				url: url,
				type: "POST",
				data: fd,
				async: false,
				dataType: 'json',
				cache: false,
				contentType: false,
                processData: false,
				success: function(data) {
					window.location.href = redirect;
				}
			});
	 	}

	 @endif

	 var biding_start = new Date("{{$data['project'][0]->biding_start}}").getTime();

	 	@if ($data['project'][0]->status == 0)
      	// Set the date we're counting down to
      		var countDownDate = new Date("{{$data['project'][0]->biding_start}}").getTime();
      		document.getElementById("label-bid").innerHTML = "Bid Starts In"
      	@elseif ($data['project'][0]->status == 1)
      		var countDownDate = new Date("{{$data['project'][0]->biding_finish}}").getTime();
      	@endif

		// Date NOW
		var today = new Date("{{date('Y-m-d H:i:s')}}");
      	// Update the count down every 1 second
      	var x = setInterval(function(){
            
            // Update seconds every one second
            today.setSeconds(today.getSeconds() + 1);
            
    		var msPerDay = 24 * 60 * 60 * 1000 ;
    		var distance = (countDownDate - today.getTime());
    		// Time calculations for days, hours, minutes and seconds
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      		// Output the result in an element with id="demo"
      		document.getElementById("days").innerHTML = days;
      		document.getElementById("hours").innerHTML = ('0' + hours).slice(-2);
      		document.getElementById("minutes").innerHTML = ('0' + minutes).slice(-2);
      		document.getElementById("seconds").innerHTML = ('0' + seconds).slice(-2);
    		// document.getElementById("countdown").innerHTML = days + "d " + hours + "h "+ minutes + "m " + seconds + "s ";
    		// If the count down is over, write some text 
    		if (distance < 0) {
        	clearInterval(x);
      		document.getElementById('clockdiv').remove();
      		// Refresh page setelah countdown selesai bid starts in
      		@if(Auth::user()->user_vendor != NULL)
				setTimeout(function(){
					window.location.reload();
				}, 5000);
			@endif

      		@if(Auth::user()->user_role == "USER")
      			@if (in_array($data['project'][0]->status, array(4)))
        			document.getElementById('btn-submit-bid').remove();
      				document.getElementById('btn-bid').remove();
      				document.getElementById('btn-revise').remove();
      				document.getElementById('btn-submit-revise').remove();
      			@endif
			@elseif(Auth::user()->user_role == "ADMIN")
				var fd = new FormData();
					fd.append('id', ($("#project_id").val()));
					fd.append('status', 4);
				var url = "{{url('/')}}/project/mailToProc";
      		@endif
      		

			@if ($data['project'][0]->status == 0)
				var url = "{{url('/')}}/project/startBid";
				var fd = new FormData();
				fd.append('project_id', ($("#project_id").val()));
				// window.onload = function () {
    // 				if (! localStorage.justOnce) {
    //     				localStorage.setItem("justOnce", "true");
    // 				}
				// }

			@endif
      		$.ajaxSetup({
    			headers:
    			{
        			'X-CSRF-Token': $('input[name="_token"]').val()
    			}
				});
			
				$.ajax({
					url: url,
					type: "POST",
					data: fd,
					async: false,
					dataType: 'json',
					cache: false,
					contentType: false,
                	processData: false,
                	success: function(data) {
						setTimeout(function(){
   							window.location.reload()
							}, 3000);
   						}
					}
				);
    		}
      	}, 1000);

      	 function chooseVendor(value)
      	 {
      	 	swal({
            	title: "Anda yakin?",
            	text: "Setelah dikirim, pilihan Vendor tidak dapat diubah.",
            	type: "warning",
            	showCancelButton: true,
            	confirmButtonColor: "#DD6B55",
            	confirmButtonText: "Ya, saya yakin!",
            	cancelButtonText: "Tidak, mohon dibatalkan!",
            	closeOnConfirm: false,
            	closeOnCancel: false,
            	closeOnClickOutside: false,
        	},
        
        	function(isConfirm){
            	if (isConfirm) {
                	swal({
                			title: 'Awesome',
                        	text: 'Vendor telah ditentukan.',
                        	type: 'success',
                        	timer: 1500,
                        	showConfirmButton: false
                    	}, function() {
                        	var fd = new FormData();
							fd.append('tpv_id', value);

							var url = "{{url('/')}}/project-vendor/winner";
							$.ajaxSetup({
    						headers:
    						{
        						'X-CSRF-Token': $('input[name="_token"]').val()
    						}
							});
			
							$.ajax({
								url: url,
								type: "POST",
								data: fd,
								async: false,
								dataType: 'json',
								cache: false,
								contentType: false,
                				processData: false,
                				success: function(data) {
								window.location.href = "{{url('/')}}/project/view/"+$("#project_id").val();
					}
							});
                    	});
            	} else {
                	swal({
                        	title: 'Dibatalkan',
                        	text: 'Silakan koreksi kembali :)',
                        	type: 'error',
                        	timer: 1500,
                        	showConfirmButton: false
                	});
            	}
        	});
      	 }

      	 @if($data['project'][0]->status == 1)

      	 	var timeout = setTimeout(function(){ location.reload(); }, 7000);
      	 	function removeMeta()
      	 	{
      	 		clearTimeout(timeout);

      	 	}
      	 @endif

      	@if(Auth::user()->user_vendor != NULL && !empty($data['pv']))
			@if($data['pv'][0]->status == 1)
				@if (count($data['history']) == $data['max_bid'])
					$("#btn-bid").attr('disabled', 'true');
				@endif
			@endif
		@endif

		function needReview(project)
		{
			window.location.href="{{url('project/need-review/')}}/"+project;
		}

		function acceptOrRevise(project, status_review)
		{
			var desc = document.getElementById("review_desc").value;
			window.location.href="{{url('project/accept-or-revise?id=')}}"+project+"&status="+status_review+"&desc="+desc;	
		}

		function removeAttrDisabled()
		{
			var input = document.getElementById("review_desc");
			var len = input.value.length;
			
			if (len >= 5) {
				$("#btn-accept-project").removeAttr("disabled");
        		$("#btn-revise-project").removeAttr("disabled");
			} else {
				$("#btn-accept-project").attr("disabled", true);
        		$("#btn-revise-project").attr("disabled", true);
			}
		}
		// $("#review_desc").keyup(function(){
        	
		// });
</script>
@endsection
