@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<!-- <h1 class="text-primary">Registrasi Vendor</h1> -->
	</div><!--end .col -->
	<!-- <div class="col-lg-3 col-md-4">
		<article class="margin-bottom-xxl">
			<p class="lead">
				Material Admin uses by default a vertical form layout.
				In a vertical form the label is placed above the input field.
			</p>
			<p>
				When the field is focused, there will be a thicker line drawn beneath it.
				The label in this example is always visible.
			</p>
		</article>
	</div> -->
	<!--end .col -->
	<div class="col-md-12">
		{{ Form::open(array('url'=>'project/store-project', 'class'=>'form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
		{{ csrf_field() }}
			<div class="card">
				<div class="card-head style-primary">
					<header>Tambah Project Baru</header>
				</div>
				<!-- START CARD BODY -->
				<div class="card-body floating-label">
					<div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
									<select name="company" class="form-control" id="company">
										<option>-- Silakan Pilih --</option>
										@foreach($data['company'] as $company)
										<option value="{{$company->id}}" @if(old('company') == $company->id) selected @endif>{{$company->company_name}}</option>
										@endforeach
									</select>
								<label for="company">
									{!! required('Perusahaan') !!}
								</label>
									@if ($errors->has('company'))
										<span class="help-block">
											<strong>{{ $errors->first('company') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('site') ? ' has-error' : '' }}">
									<input type="text" name="site" class="form-control" id="site" value="{{ old('site') }}">
									<label for="site">
										Site
									</label>
									@if ($errors->has('site'))
										<span class="help-block">
											<strong>{{ $errors->first('site') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('project_name') ? ' has-error' : '' }}">
									<input type="text" name="project_name" class="form-control" id="project_name" value="{{ old('project_name') }}">
									<label for="project_name">
										{!! required('Nama Projek') !!}
									</label>
									@if ($errors->has('project_name'))
										<span class="help-block">
											<strong>{{ $errors->first('project_name') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group{{ $errors->has('project_type') ? ' has-error' : '' }}">
									<select name="project_type" id="project_type" class="form-control">
										<option>-- Silakan Pilih --</option>
										@foreach($data['project_type'] as $type)
										<option value="{{$type->id}}+{{$type->lookup_value}}" @if(old('project_type') == $type->id) selected @endif>{{$type->desc}}</option>
										@endforeach
									</select>									
									<input type="hidden" name="validation_type" value="" id="validation_type">
									<label for="project_type">
										{!! required('Jenis Projek') !!}
									</label>
									@if ($errors->has('project_type'))
										<span class="help-block">
											<strong>{{ $errors->first('project_type') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
							<select name="category[]" class="form-control select2-list" id="category" multiple>
								@foreach($data['category'] as $category)
								<option value="{{$category->id}}" @if(old('category')) @if( in_array($category->id, old('category'))) selected @endif @endif>{{$category->category_name}}</option>
								@endforeach
							</select>
							<label for="category">
								{!! required('Kategori Perusahaan') !!}
							</label>
							@if ($errors->has('category'))
								<span class="help-block">
									<strong>{{ $errors->first('category') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<input type="text" name="description" class="form-control" id="description" value="{{ old('description') }}">
							<label for="description">
								{!! required('Deskripsi Projek') !!}
							</label>
							@if ($errors->has('description'))
								<span class="help-block">
									<strong>{{ $errors->first('description') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group{{ $errors->has('description_of_place') ? ' has-error' : '' }}">
							<input type="text" name="description_of_place" class="form-control" id="description_of_place" value="{{ old('description_of_place') }}">
							<label for="description_of_place">
								{!! required('Deskripsi Daerah Projek') !!}
							</label>
							@if ($errors->has('description_of_place'))
								<span class="help-block">
									<strong>{{ $errors->first('description_of_place') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group{{ $errors->has('project_range') ? ' has-error' : '' }}">
							<input type="text" name="project_range" class="form-control" id="project_range" value="{{ old('project_range') }}">
							<label for="project_range">
								{!! required('Jangka Waktu Pengerjaan Projek') !!}
							</label>
							@if ($errors->has('project_range'))
								<span class="help-block">
									<strong>{{ $errors->first('project_range') }}</strong>
								</span>
							@endif
						</div>
						{{-- <div class="row" id="price_project" style="display: @if(Auth::user()->user_role == "ADMIN")none @endif;">
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('price_lower_limit') ? ' has-error' : '' }}">
									<input type="text" name="price_lower_limit" class="form-control" id="price_lower_limit" value="{{ old('price_lower_limit') }}">
									<label for="price_lower_limit">
										{!! required('Harga Batas Bawah') !!}
									</label>
									@if ($errors->has('price_lower_limit'))
										<span class="help-block">
											<strong>{{ $errors->first('price_lower_limit') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('price_upper_limit') ? ' has-error' : '' }}">
									<input type="text" name="price_upper_limit" class="form-control" id="price_upper_limit" value="{{ old('price_upper_limit') }}">
									<label for="price_upper_limit">
										{!! required('Harga Batas Atas') !!}
									</label>
									@if ($errors->has('price_upper_limit'))
										<span class="help-block">
											<strong>{{ $errors->first('price_upper_limit') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group{{ $errors->has('multiples_value') ? ' has-error' : '' }}">
									<input type="text" name="multiples_value" class="form-control" id="multiples_value" value="{{ old('multiples_value') }}">
									<label for="multiples_value">
										{!! required('Nilai Kelipatan') !!}
									</label>
									@if ($errors->has('multiples_value'))
										<span class="help-block">
											<strong>{{ $errors->first('multiples_value') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div> --}}
						{{-- <div class="row">
							<div class="col-md-12">
								<div class="form-group{{ $errors->has('price_component') ? ' has-error' : '' }}">
									<input type="text" name="price_component" class="form-control" id="price_component" value="{{ old('price_component') }}" data-role="tagsinput">
									<label for="price_component">
										{!! required('Komponen Harga') !!}
									</label>
									@if ($errors->has('price_component'))
										<span class="help-block">
											<strong>{{ $errors->first('price_component') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div> --}}
						<div class="form-group{{ $errors->has('biding_range') ? ' has-error' : '' }}">
							<input type="text" name="biding_range" class="form-control" id="biding_range" value="{{ old('biding_range') }}">
							<label for="biding_range">
								{!! required('Jangka Waktu Penawaran') !!}
							</label>
							@if ($errors->has('biding_range'))
								<span class="help-block">
									<strong>{{ $errors->first('biding_range') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="attachment" class="control-label">Lampiran</label>
							</div>
							<div class="col-sm-10">
								<div class="form-group{{ $errors->has('attachment') ? ' has-error' : '' }}">
									<input type="file" name="attachment[]" class="form-control" id="attachment" multiple>
									@if ($errors->has('attachment'))
									<span class="help-block">
										<strong>{{ $errors->first('attachment') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CARD BDDY -->
				<div class="card-actionbar">
					<div class="card-actionbar-row">
						<button type="reset" class="btn btn-danger ink-reaction">Reset</button>
						<button type="submit" class="btn btn-primary ink-reaction">Kirim</button>
					</div>
				</div>
			</div>
			<em class="text-caption">Label yang bertanda (<font style="color: red;">*</font>) wajib diisi.</em>
		{{ Form::close() }}
	</div>
</div>
<script type="text/javascript">
	$("#company").change(function(){
		var selectedCompany = $("#company").val();
		var url = "{{ url('/') }}/site/site-company/"+selectedCompany;
		$.get(url,
		function(data) {
			var sites = $("#site");
			sites.empty();
			sites .append($("<option/>").text("-- Silakan Pilih --"));
			$.each(data, function(key, value) {   
				sites .append($("<option/>").attr("value", key).text(value));
			});
		});
	});

	$('#project_range').daterangepicker({});
	$('#biding_range').daterangepicker({
		timePicker: true,
        locale: {
            format: 'MM/DD/YYYY hh:mm A',
        }
	});
	$('#project_range').on('cancel.daterangepicker', function(ev, picker) {
  	//do something, like clearing an input
  		$('#project_range').val('');
	});
	$('#biding_range').on('cancel.daterangepicker', function(ev, picker) {
  	//do something, like clearing an input
  		$('#biding_range').val('');
	});

	$("#category").select2();
	$("#price_component").tagsinput();

	// $("#project_type").on('change', function(){
	// 	var validation_type = this.value.split("+")[1];

	// 	if (validation_type == 2) {
	// 		$("#price_project").attr('style', 'display:none');
	// 	}
	// });
</script>
@endsection