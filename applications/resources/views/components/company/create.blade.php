@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
	{{ Form::open(array('url'=>'company/store-company', 'class'=>'form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
		{{ csrf_field() }}
		<div class="card">
			<div class="card-head style-primary">
				<header>Tambah Perusahaan</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
						<input type="text" name="company_name" class="form-control" id="company_name" value="{{old('company_name')}}">
						<label for="company_name">
							{!! required('Nama Perusahaan') !!}
						</label>
						@if ($errors->has('company_name'))
							<span class="help-block">
								<strong>{{ $errors->first('company_name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
						<input type="text" name="remarks" class="form-control" id="remarks" value="{{old('remarks')}}">
						<label for="remarks">
							Catatan
						</label>
						@if ($errors->has('remarks'))
							<span class="help-block">
								<strong>{{ $errors->first('remarks') }}</strong>
							</span>
						@endif
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="reset" class="btn btn-danger ink-reaction">Reset</button>
					<button type="submit" class="btn btn-primary ink-reaction">Kirim</button>
				</div>
			</div>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection