@extends('layouts.app')
@section('content')
<?php
$no = 0;
?>
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Daftar Perusahaan</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<center>
						<span>
							<a href="{{url('company/create')}}" class="btn btn-info ink-reaction"><i class="fa fa-plus"></i> Tambah</a>
						</span>
					</center><br>
					<span class="divider"></span>
					<table class="table table-responsive">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Perusahaan</th>
								<th>Catatan</th>
								<th>Tools</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data['company'] as $company)
							<tr>
								<th>{{++$no}}.</th>
								<td>{{$company->company_name}}</td>
								<td>{{$company->remarks}}</td>
								<td><a href="{{url('company/view/')}}/{{$company->id}}" class="btn ink-reaction btn-floating-action btn-warning"><i class="fa fa-eye"></i></a></td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection