@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
	{{ Form::open(array('url'=>'company/store-company', 'class'=>'form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
		{{ csrf_field() }}
		<div class="card">
			<div class="card-head style-primary">
				<header>Rincian Perusahaan &raquo; {{$data['company'][0]->company_name}}</header>
			</div>
			<div class="card-body floating-label">
				<div class="form-group">
					<label for="company_name">
						<h4><b>Nama Perusahaan</b>: {{$data['company'][0]->company_name}}</h4>
					</label>
				</div>
				<div class="form-group">
					<label for="remarks">
						<h4><b>Catatan</b>: {{$data['company'][0]->remarks}}</h4>
					</label>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					
				</div>
			</div>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection