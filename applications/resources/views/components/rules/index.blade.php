@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-primary"></h1>
            <div class="panel panel-default">
                <div class="panel-heading">Aturan & Prasyarat - eProcurement</div>

                <div class="panel-body">
                    <marquee direction="up">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</marquee>
                </div>
            </div>
        </div><!--end .col -->
    </div>
@endsection
