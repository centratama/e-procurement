@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-md-12">
		{{ Form::open(array('url'=>'password/store-password', 'class'=>'form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
		{{ csrf_field() }}
		<div class="card">
			<div class="card-head style-primary">
				<header>Pengguna &raquo; {{$data['users'][0]->name}}</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<div class="row">
						<div class="col-md-2">
							<b>Nama</b>:
						</div>
						<div class="col-md-4">
							{{$data['users'][0]->name}}
						</div>
						<div class="col-md-2">
							<b>Alamat Surel</b>:
						</div>
						<div class="col-md-4">
							{{$data['users'][0]->email}}
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<input type="password" name="password" class="form-control" id="password">
								<label for="password">
									{!! required('Sandi Baru') !!}
								</label>
								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								<input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
								<label for="password_confirmation">
									{!! required('Konfirmasi Sandi') !!}
								</label>
								@if ($errors->has('password_confirmation'))
									<span class="help-block">
										<strong>{{ $errors->first('password_confirmation') }}</strong>
									</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="reset" class="btn btn-danger ink-reaction">Reset</button>
					<button type="submit" class="btn btn-primary ink-reaction">Kirim</button>
					<input type="hidden" name="id" value="{{$data['users'][0]->id}}">
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
@endsection