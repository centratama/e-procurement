@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
	{{ Form::open(array('url'=>'site/store-site', 'class'=>'form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
		{{ csrf_field() }}
		<div class="card">
			<div class="card-head style-primary">
				<header>Tambah Site</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<div class="form-group{{ $errors->has('site_id') ? ' has-error' : '' }}">
						<input type="text" name="site_id" class="form-control" id="site_id" value="{{old('site_id')}}">
						<label for="site_id">
							{!! required('ID Site') !!}
						</label>
						@if ($errors->has('site_id'))
							<span class="help-block">
								<strong>{{ $errors->first('site_id') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('site_name') ? ' has-error' : '' }}">
						<input type="text" name="site_name" class="form-control" id="site_name" value="{{old('site_name')}}">
						<label for="site_name">
							{!! required('Nama Site') !!}
						</label>
						@if ($errors->has('site_name'))
							<span class="help-block">
								<strong>{{ $errors->first('site_name') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
						<select name="company" class="form-control" id="company">
							<option>-- Silakan Pilih --</option>
							@foreach($data['company'] as $company)
							<option value="{{$company->id}}" @if(old('company') == $company->id) selected @endif>{{$company->company_name}}</option>
							@endforeach
							</select>
							<label for="company">
								{!! required('Perusahaan') !!}
							</label>
						@if ($errors->has('company'))
							<span class="help-block">
								<strong>{{ $errors->first('company') }}</strong>
							</span>
						@endif
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="reset" class="btn btn-danger ink-reaction">Reset</button>
					<button type="submit" class="btn btn-primary ink-reaction">Kirim</button>
				</div>
			</div>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection