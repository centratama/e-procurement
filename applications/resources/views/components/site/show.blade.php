@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
		{{ csrf_field() }}
		<div class="card">
			<div class="card-head style-primary">
				<header>Rincian Site &raquo; {{$data['site'][0]->site_name}}</header>
			</div>
			<div class="card-body floating-label">
				<div class="row">
					<div class="col-md-2">
						<b>Site ID</b>:
					</div>
					<div class="col-md-4">
						{{$data['site'][0]->site_id}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Nama Site</b>:
					</div>
					<div class="col-md-4">
						{{$data['site'][0]->site_name}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Entitas</b>:
					</div>
					<div class="col-md-4">
						{{$data['site'][0]->company_name}}
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection