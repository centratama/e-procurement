@extends('layouts.app')
@section('content')
<?php
$no = 0;
?>
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Daftar Site</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<center>
						<span>
							<a href="{{url('site/create')}}" class="btn btn-info ink-reaction"><i class="fa fa-plus"></i> Tambah</a>
						</span>
					</center><br>
					<span class="divider"></span>
					<div class="table-responsive">
						<table id="datatable1" class="table table-striped table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Site</th>
									<th>Nama Site</th>
									<th>Tools</th>
								</tr>
							</thead>
							<tbody>
							@foreach($data['site'] as $site)
								<tr>
									<th>{{++$no}}.</th>
									<td>{{$site->site_id}}</td>
									<td>{{$site->site_name}}</td>
									<td>
										<a href="{{url('site/view/')}}/{{$site->site_id}}" class="btn ink-reaction btn-floating-action btn-warning"><i class="fa fa-eye"></i></a>
										<a href="{{url('site/edit/')}}/{{$site->site_id}}" class="btn btn-floating-action btn-success ink-reaction right"><i class="fa fa-pencil"></i></a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection