@extends('layouts.app')
@section('content')
<?php
$no = 0;
?>
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Daftar Vendor</header>
			</div>
			<div class="card-body floating-label">
				<div>
					<span class="divider"></span>
					<table class="table table-responsive">
						<thead>
							<tr>
								<th>#</th>
								<th>Vendor</th>
								<th>Alamat</th>
								<th>Provinsi</th>
								<th>Kota</th>
								<th>Status</th>
								<th>Kategori Vendor</th>
								<th>Tools</th>
							</tr>
						</thead>
						<tbody>
						@foreach($data['vendor'] as $key => $vendor)
							<tr>
								<th>{{++$no}}.</th>
								<td>{{$vendor->vendor_name}}</td>
								<td>{{$vendor->vendor_address}}</td>
								<td>{{$vendor->province_name}}</td>
								<td>{{$vendor->city_name}}</td>
								<td>{!! statusVendor($vendor->is_active) !!}</td>
								<td>{!! $data['category'][$key] !!}</td>
								<td>
									<a href="{{url('vendor/view/')}}/{{$vendor->id}}" class="btn"><i class="md md-search"></i></a>
									<a href="{{url('vendor/edit/')}}/{{$vendor->id}}" class="btn"><i class="fa fa-pencil"></i></a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection