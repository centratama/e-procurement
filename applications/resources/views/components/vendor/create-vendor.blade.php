@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<!-- <h1 class="text-primary">Registrasi Vendor</h1> -->
	</div><!--end .col -->
	<!-- <div class="col-lg-3 col-md-4">
		<article class="margin-bottom-xxl">
			<p class="lead">
				Material Admin uses by default a vertical form layout.
				In a vertical form the label is placed above the input field.
			</p>
			<p>
				When the field is focused, there will be a thicker line drawn beneath it.
				The label in this example is always visible.
			</p>
		</article>
	</div> -->
	<!--end .col -->
	<div class="col-md-12">
		{{ Form::open(array('url'=>'vendor/store-vendor', 'class'=>'form', 'enctype' => 'multipart/form-data', 'method' => 'post')) }}
		{{ csrf_field() }}
			<div class="card">
				<div class="card-head style-primary">
					<header>Registrasi Vendor Baru</header>
				</div>
				<!-- START CARD BODY -->
				<div class="card-body floating-label">
					<div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('vendor_type') ? ' has-error' : '' }}">
									<select name="vendor_type" class="form-control" id="vendor_type">
										<option>-- Silakan Pilih --</option>
										@foreach($data['vendor_type'] as $type)
										<option value="{{$type->lookup_value}}" @if(old('vendor_type') == $type->lookup_value) selected @endif>{{$type->lookup_desc}}</option>
										@endforeach
									</select>
									<label for="vendor_type">
										{!! required('Jenis Perusahaan') !!}
									</label>
									@if ($errors->has('vendor_type'))
										<span class="help-block">
											<strong>{{ $errors->first('vendor_type') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
									<select name="category[]" class="form-control select2-list" id="category" multiple>
										@foreach($data['category'] as $category)
										<option value="{{$category->id}}" @if(old('category') == $category->id) selected @endif>{{$category->category_name}}</option>
										@endforeach
									</select>
									<label for="category">
										{!! required('Kategori Perusahaan (<i>Multiple Selection</i>)') !!}
									</label>
									@if ($errors->has('category'))
										<span class="help-block">
											<strong>{{ $errors->first('category') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-group{{ $errors->has('vendor_name') ? ' has-error' : '' }}">
							<input type="text" name="vendor_name" class="form-control" id="vendor_name" value="{{old('vendor_name')}}">
							<label for="vendor_name">
								{!! required('Nama Perusahaan') !!}
							</label>
							@if ($errors->has('vendor_name'))
								<span class="help-block">
									<strong>{{ $errors->first('vendor_name') }}</strong>
								</span>
							@endif
						</div>
						<div class="form-group{{ $errors->has('vendor_email') ? ' has-error' : '' }}">
							<input type="email" name="vendor_email" class="form-control" id="vendor_email" value="{{ old('vendor_email') }}">
							<label for="vendor_email">
								{!! required('Alamat Surel') !!}
							</label>
							@if ($errors->has('vendor_email'))
								<span class="help-block">
									<strong>{{ $errors->first('vendor_email') }}</strong>
								</span>
							@endif
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
									<select name="province" class="form-control" id="province">
										<option>-- Silakan Pilih --</option>
										@foreach($data['province'] as $province)
										<option value="{{$province->id}}" @if(old('province') == $province->id) selected @endif>{{$province->province_name}}</option>
										@endforeach
									</select>
									<label for="province">
										{!! required('Provinsi') !!}
									</label>
									@if ($errors->has('province'))
										<span class="help-block">
											<strong>{{ $errors->first('province') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
									<select name="city" class="form-control" id="city">
										<option>Pilih Provinsi Terlebih Dahulu</option>
									</select>
									<label for="city">
										{!! required('Kota') !!}
									</label>
									@if ($errors->has('city'))
										<span class="help-block">
											<strong>{{ $errors->first('city') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<input type="text" name="address" class="form-control" id="address" value="{{old('address')}}">
							<label for="address">
								{!! required('Alamat Perusahaan') !!}
							</label>
							@if ($errors->has('address'))
								<span class="help-block">
									<strong>{{ $errors->first('address') }}</strong>
								</span>
							@endif
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('zipcode') ? ' has-error' : '' }}">
									<input type="text" name="zipcode" class="form-control" id="zipcode" value="{{old('zipcode')}}">
									<label for="zipcode">
										{!! required('Kodepos') !!}
									</label>
									@if ($errors->has('zipcode'))
										<span class="help-block">
											<strong>{{ $errors->first('zipcode') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group{{ $errors->has('npwp') ? ' has-error' : '' }}">
									<input type="text" name="npwp" class="form-control" id="npwp" value="{{old('npwp')}}">
									<label for="npwp">
										{!! required('NPWP') !!}
									</label>
									@if ($errors->has('npwp'))
										<span class="help-block">
											<strong>{{ $errors->first('npwp') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END CARD BDDY -->
				<div class="card-actionbar">
					<div class="card-actionbar-row">
						<button type="reset" class="btn btn-danger ink-reaction">Reset</button>
						<button type="submit" class="btn btn-primary ink-reaction">Kirim</button>
					</div>
				</div>
			</div>
			<em class="text-caption">Label yang bertanda (<font style="color: red;">*</font>) wajib diisi.</em>
		{{ Form::close() }}
	</div>
</div>
<script type="text/javascript">
	$("#province").change(function(){
		var selectedProvince = $("#province").val();
		var url = "{{ url('/') }}/city-province/"+selectedProvince;
		$.get(url,
		function(data) {
			var cities = $("#city");
			cities.empty();
			cities .append($("<option/>").text("-- Silakan Pilih --"));
			$.each(data, function(key, value) {   
				cities .append($("<option/>").attr("value", key).text(value));
			});
		});
	});

	jQuery(function($){
		$("#npwp").mask("99-999-999-9-999-999");
    });

    $("#category").select2();
</script>
@endsection