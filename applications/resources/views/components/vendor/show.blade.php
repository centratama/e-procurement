@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-lg-12"></div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Rincian Perusahaan &raquo; {{$data['vendor'][0]->vendor_name}}</header>
			</div>
			<div class="card-body floating-label">
				<div class="row">
					<div class="col-md-2">
						<b>Nama Vendor</b>:
					</div>
					<div class="col-md-4">
						{{$data['vendor'][0]->vendor_name}}
					</div>
					<div class="col-md-2">
						<b>Surel</b>:
					</div>
					<div class="col-md-4">
						{{$data['vendor'][0]->vendor_email}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Alamat</b>:
					</div>
					<div class="col-md-10">
						{{$data['vendor'][0]->vendor_address}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Provinsi</b>:
					</div>
					<div class="col-md-4">
						{{$data['vendor'][0]->province_name}}
					</div>
					<div class="col-md-2">
						<b>Kota</b>:
					</div>
					<div class="col-md-4">
						{{$data['vendor'][0]->city_name}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Kodepos</b>:
					</div>
					<div class="col-md-4">
						{{$data['vendor'][0]->zipcode}}
					</div>
					<div class="col-md-2">
						<b>NPWP</b>:
					</div>
					<div class="col-md-4">
						{{$data['vendor'][0]->npwp}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Kategori Vendor</b>:
					</div>
					<div class="col-md-10">
						{!! $data['category'] !!}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<b>Status</b>:
					</div>
					<div class="col-md-10">
						{!! statusVendor($data['vendor'][0]->is_active) !!}
					</div>
				</div>
			</div>

			{{ Form::model($data['vendor'], array('route' => ['update-status', $data['vendor'][0]->id], 'method' => 'patch', 'class'=>'form', 'id' => 'updateStatusForm')) }}
				<input type="hidden" name="status" id="status">
			{!! Form::close() !!}

			<div class="card-actionbar">
				<div class="card-actionbar-row">
					@if ($data['vendor'][0]->is_active == 1)
						<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deactivate Vendor" id="btn-deactivate"><i class="md md-error"></i> Deactivate</button>
					@else
						<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Activate Vendor" id="btn-activate"><i class="md md-verified-user"></i> Activate</button>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function (){
		$('#btn-deactivate').click(function(e){
			e.preventDefault();
			document.getElementById('status').value = 2;
        	swal({
            	title: "Anda yakin?",
            	text: "Vendor tersebut akan dinonaktifkan.",
            	type: "warning",
            	showCancelButton: true,
            	confirmButtonColor: "#DD6B55",
            	confirmButtonText: "Ya",
            	cancelButtonText: "Tidak",
            	closeOnConfirm: false,
            	closeOnCancel: false,
            	closeOnClickOutside: false,
        	},
        
        	function(isConfirm){
            	if (isConfirm) {
                	swal({
                        title: 'Sukses',
                        text: 'Vendor telah dinonaktifkan.',
                        type: 'success',
                        timer: 1500,
                        showConfirmButton: false
                    }, function() {
                        $('#updateStatusForm').submit();
                    });
            	} else {
                	swal({
                        title: 'Dibatalkan',
                        text: 'Proses dibatalkan.',
                        type: 'error',
                        timer: 1500,
                        showConfirmButton: false
                	});
            	}
        	});
		});

		$('#btn-activate').click(function(e){
			e.preventDefault();
			document.getElementById('status').value = 1;
        	swal({
            	title: "Anda yakin?",
            	text: "Vendor tersebut akan diaktifkan kembali.",
            	type: "warning",
            	showCancelButton: true,
            	confirmButtonColor: "#DD6B55",
            	confirmButtonText: "Ya",
            	cancelButtonText: "Tidak",
            	closeOnConfirm: false,
            	closeOnCancel: false,
            	closeOnClickOutside: false,
        	},
        
        	function(isConfirm){
            	if (isConfirm) {
                	swal({
                        title: 'Sukses',
                        text: 'Vendor telah diaktifkan.',
                        type: 'success',
                        timer: 1500,
                        showConfirmButton: false
                    }, function() {
                        $('#updateStatusForm').submit();
                    });
            	} else {
                	swal({
                        title: 'Dibatalkan',
                        text: 'Proses dibatalkan.',
                        type: 'error',
                        timer: 1500,
                        showConfirmButton: false
                	});
            	}
        	});
		});
	});
</script>
@endsection