Yth : <b>{{$data['vendor'][0]->vendor_name}}</b>,<br>

<p>Dokumen persyaratan yang telah anda unggah pada laman <a href="{{$data['link']}}">{{$data['project'][0]->project_name}}</a> <b>telah terverifikasi</b>.</p>
<p>Proses bidding akan dimulai pada {{date("d M, Y h:i A", strtotime($data['project'][0]->biding_start))}}.</p>

Hormat kami,<br><br>
CENTRATAMA GROUP<br>
TCC Batavia Tower One lt. 16,<br>
Jl. KH. Mas Mansyur Kav. 126<br>
Jakarta Pusat 10220<br><br>
Tel: <u>+62 21 29529404</u><br>
Fax: <u>+62 21 29678232</u><br>