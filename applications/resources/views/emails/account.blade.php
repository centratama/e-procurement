Yth : <b>{{$data['name']}}</b>,<br><br>

<p>Akun pengguna Anda berhasil terverifikasi, berikut adalah informasi <i>username</i> dan <i>password</i> yang digunakan untuk <i>login</i> ke aplikasi E-Procurement:</p>

<ul>
	<li><i>Username</i>: {{$data['username']}}</li>
	<li><i>Password</i>: {{$data['password']}}</li>
</ul>


<p><b>Mohon untuk menjaga kerahasiaan informasi akun pengguna ini. Silakan gunakan fitur <i>Change Password</i> jika ingin mengubah <i>Password</i></b></p>

Hormat kami,<br><br>
CENTRATAMA GROUP<br>
TCC Batavia Tower One lt. 16,<br>
Jl. KH. Mas Mansyur Kav. 126<br>
Jakarta Pusat 10220<br><br>
Tel: <u>+62 21 29529404</u><br>
Fax: <u>+62 21 29678232</u><br>