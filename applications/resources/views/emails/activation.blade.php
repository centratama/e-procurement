Yth : <b>{{$data['name']}}</b>,<br><br>

<p>Terima kasih telah melakukan registrasi pada aplikasi E-Procurement. Untuk dapat masuk ke dalam Aplikasi mohon Klik Tautan di bawah ini untuk Verifikasi email yang digunakan dan Anda akan mendapatkan <i>Username</i> dan <i>Password</i>.</p>

<p><b>Tautan verifikasi:</b><br>
<a href="{{$data['link']}}">{{$data['link']}}</a></p>

Hormat kami,<br><br>
CENTRATAMA GROUP<br>
TCC Batavia Tower One lt. 16,<br>
Jl. KH. Mas Mansyur Kav. 126<br>
Jakarta Pusat 10220<br><br>
Tel: <u>+62 21 29529404</u><br>
Fax: <u>+62 21 29678232</u><br>