Yth : <b>{{$data['name']}}</b>,<br><br>

<p>{{$data['company']}} akan mengadakan penawaran untuk pekerjaan {{$data['project_name']}}.</p>
<p>Berikut adalah rincian informasi pekerjaannya:</p>

<ul>
	<li>Pekerjaan: <b>{{$data['project_name']}} @if ($data['site'] != null)untuk Site {{$data['site']}} @endif</b></li>
	<li>Deskripsi Pekerjaan: <b>{{$data['desc']}}</b></li>
	<li>Jangka Waktu Pengerjaan: <b>{{date("d M, Y", strtotime($data['project_start_date']))}} s/d {{date("d M, Y", strtotime($data['project_end_date']))}}</b></li>
	<li>Waktu Proses <i>Bidding</i>: <b>{{date("d M, Y h:i A", strtotime($data['biding_start']))}} s/d {{date("d M, Y h:i A", strtotime($data['biding_finish']))}}</b></li>
</ul>

<p>Jika tertarik, silakan unduh file attachment yang terlampir dan mohon dilengkapi. Jika sudah dilengkapi, silakan untuk klik tautan dibawah ini untuk mengunggah dan mengikuti proses <i>Biding Project</i>.</p>
<a href="{{$data['link']}}">{{$data['link']}}</a><br><br>
<p><b><i>Note:</i> Berkas yang dapat diunggah hanya dengan format PDF atau Image (JPEG, PNG).</b></p>

Hormat kami,<br><br>
{{$data['company']}}<br>
TCC Batavia Tower One lt. 16,<br>
Jl. KH. Mas Mansyur Kav. 126<br>
Jakarta Pusat 10220<br><br>
Tel: <u>+62 21 29529404</u><br>
Fax: <u>+62 21 29678232</u><br>