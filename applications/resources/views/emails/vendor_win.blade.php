Yth : <b>{{$data['vendor']->vendor_name}}</b>,<br><br>

<font>Terima kasih atas partisipasinya dalam mengikuti penawaran {{$data['project'][0]->project_name}}. <b>Selamat!</b> {{$data['vendor']->vendor_name}} terpilih sebagai pemenang pada kesempatan ini. Anda menempati peringkat {{$data['last_price'][0]->rank}} dari {{$data['count_pv']}} peserta yang mengikuti penawaran ini.</font><br>
<font>Berikut adalah rincian penawaran terakhir dari {{$data['vendor']->vendor_name}}:</font>
<br>
<table>
	<thead>
		<th>#</th>
		<th>Komponen</th>
		<th>Harga</th>
	</thead>
	<tbody>
		@php ($i = 0)
		@foreach($data['price_comp'] as $key => $comp)
			<tr>
				<th>{{++$i}}.</th>
				<td>{{$comp->component}}</td>
				<td align="right">{{number_format($data['last_price_comp'][$key], 2, ',', '.')}}</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th colspan="2">Total</th>
			<td>{{number_format($data['last_price'][0]->prices_bid, 2, ',', '.')}}</td>
		</tr>
	</tfoot>
</table>
<br>
<font>Kami akan menginformasikan hal-hal yang diperlukan untuk proses selanjutnya.</font>
<br>
<br>

<font>Terima kasih.</font><br><br>
Hormat kami,<br>
{{$data['project'][0]->company_name}}<br>
TCC Batavia Tower One lt. 16,<br>
Jl. KH. Mas Mansyur Kav. 126<br>
Jakarta Pusat 10220<br><br>
Tel: <u>+62 21 29529404</u><br>
Fax: <u>+62 21 29678232</u><br>