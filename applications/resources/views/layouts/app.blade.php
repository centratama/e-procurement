<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Centratama Group - eProcurement</title>

        <!-- BEGIN META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="your,keywords">
        <meta name="description" content="Short explanation about this website">
        @if (Auth::guest())
        @else
            {{ csrf_field() }}
        @endif
        <!-- END META -->

        <!-- BEGIN STYLESHEETS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
        {!! Html::script('assets/materialadmin/assets/js/libs/jquery/jquery-1.11.2.min.js') !!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/bootstrap.css?1422792965')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/materialadmin.css?1425466319')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/font-awesome.min.css?1422529194')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/material-design-iconic-font.min.css?1421434286')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/rickshaw/rickshaw.css?1422792967')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/morris/morris.core.css?1420463396')!!}
        {!! Html::style('assets/swal/dist/sweetalert.css') !!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/select2/select2.css?1424887856')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/multi-select/multi-select.css?1424887857')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/bootstrap-tagsinput/bootstrap-tagsinput.css?1424887862')!!}
        {!! Html::style('assets/materialadmin/assets/css/theme-default/libs/typeahead/typeahead.css?1424887863')!!}
        {!! Html::script('assets/materialadmin/assets/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/typeahead/typeahead.bundle.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/multi-select/jquery.multi-select.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/select2/select2.min.js') !!}
        {!! Html::script('assets/swal/dist/sweetalert.min.js') !!}
        {!! Html::script('assets/jquery.maskedinput.min.js') !!}
        <!-- Include Date Range Picker -->
        {!! Html::script('assets/bootstrap-daterangepicker/moment.min.js') !!}
        {!! Html::script('assets/bootstrap-daterangepicker/daterangepicker.js') !!}
        {!! Html::style('assets/bootstrap-daterangepicker/daterangepicker.css') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/DataTables/jquery.dataTables.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/demo/DemoTableDynamic.js') !!}
        <!-- END STYLESHEETS -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
        <script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
        <![endif]-->
    </head>
    <body class="menubar-hoverable header-fixed menubar-pin">

        <!-- BEGIN HEADER-->
        <header id="header" >
            <div class="headerbar">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="headerbar-left">
                    <ul class="header-nav header-nav-options">
                        <li class="header-nav-brand" >
                            <div class="brand-holder">
                                <a href="{{url('/')}}">
                                    <img src="{{url('/')}}/assets/logo1.png">
                                    <!-- <span class="text-lg text-bold text-primary">E-PROCUREMENT</span> -->
                                </a>
                            </div>
                        </li>
                        <li>
                            <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="headerbar-right">
                    <ul class="header-nav header-nav-profile">
                    @if (Auth::guest())
                    @else
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                                <img src="{{url('/')}}/assets/materialadmin/assets/img/avatar1.jpg?1403934956" alt="" />
                                <span class="profile-info">
                                    {{ Auth::user()->name }}
                                    <small>{{ Auth::user()->role }}</small>
                                </span>
                            </a>
                            <ul class="dropdown-menu animation-dock">
                                <li><a href="{{url('password/change')}}"><i class="fa fa-fw fa-key"></i> Change Password</a></li>
                                <li><a href="{{url('logout')}}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                            </ul><!--end .dropdown-menu -->
                        </li><!--end .dropdown -->
                    </ul><!--end .header-nav-profile -->
                </div><!--end #header-navbar-collapse -->
                @endif
            </div>
        </header>
        <!-- END HEADER-->

        <!-- BEGIN BASE-->
        <div id="base">

            <!-- BEGIN OFFCANVAS LEFT -->
            <div class="offcanvas">
            </div><!--end .offcanvas-->
            <!-- END OFFCANVAS LEFT -->

            <!-- BEGIN CONTENT-->
            <div id="content">
                <section>
                    <div class="section-header">
                        <ol class="breadcrumb">
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>


                    <div class="section-body contain-lg">

                        @yield('content')
                        <!-- BEGIN INTRO -->
                    </div>
                </section>
            </div><!--end #content-->
            <!-- END CONTENT -->

            <!-- BEGIN MENUBAR-->
            <div id="menubar" class="menubar-inverse ">
                <div class="menubar-fixed-panel">
                    <div>
                        <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="expanded">
                        <a href="{{url('/')}}">
                            <span class="text-lg text-bold text-primary ">E-PROCUREMENT</span>
                        </a>
                    </div>
                </div>
                <div class="menubar-scroll-panel">
                    <!-- BEGIN MAIN MENU -->
                    <ul id="main-menu" class="gui-controls">
                        @if (Auth::guest())
                        <!-- BEGIN DASHBOARD -->
                        <li class="{{ setActive('/') }}">
                            <a href="{{url('/')}}">
                                <div class="gui-icon"><i class="md md-home"></i></div>
                                <span class="title">Dashboard</span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END DASHBOARD -->

                        <!-- BEGIN LOGIN -->
                        <li class="divider"></li>
                        <li class="{{ setActive('news') }}">
                            <a href="{{ url('news') }}">
                                <div class="gui-icon"><i class="fa fa-newspaper-o"></i></div>
                                <span class="title">Berita</span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END LOGIN -->

                        <!-- BEGIN LOGIN -->
                        <li class="divider"></li>
                        <li class="{{ setActive('manual-book') }}">
                            <a href="{{ url('manual-book') }}">
                                <div class="gui-icon"><i class="md md-web"></i></div>
                                <span class="title">Panduan Pengguna</span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END LOGIN -->

                        <!-- BEGIN LOGIN -->
                        <li class="divider"></li>
                        <li class="{{ setActive('vendor/regist') }}">
                            <a href="{{ url('vendor/regist') }}">
                                <div class="gui-icon"><i class="md md-book"></i></div>
                                <span class="title">Registrasi Vendor Baru</span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END LOGIN -->

                        <!-- BEGIN LOGIN -->
                        <li class="divider"></li>
                        <li class="{{ setActive('vendor-blacklist') }}">
                            <a href="{{ url('vendor-blacklist') }}">
                                <div class="gui-icon"><i class="fa fa-user-times"></i></div>
                                <span class="title">Perusahaan Blacklist</span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END LOGIN -->

                        <!-- BEGIN LOGIN -->
                        <li class="divider"></li>
                        <li class="{{ setActive('rules') }}">
                            <a href="{{ url('rules') }}">
                                <div class="gui-icon"><i class="fa fa-list-ul"></i></div>
                                <span class="title">Aturan & Prasyarat</span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END LOGIN -->

                        <!-- BEGIN LOGIN -->
                        <li class="divider"></li>
                        <li class="{{ setActive('login') }}">
                            <a href="{{ url('login') }}">
                                <div class="gui-icon"><i class="md md-person"></i></div>
                                <span class="title">Login</span>
                            </a>
                        </li><!--end /menu-li -->
                        <!-- END LOGIN -->
                    @else
                            <!-- BEGIN EMAIL -->
                        @if (in_array(Auth::user()->user_role, array("SUPER ADMIN", "ADMIN")))
                            <li class="gui-folder {{setActive(['company', 'category', 'project', 'site', 'vendor'])}}">
                                <a>
                                    <div class="gui-icon"><i class="fa fa-database"></i></div>
                                    <span class="title">Master</span>
                                </a>
                                <!--start submenu -->
                                <ul>
                                    @if (Auth::user()->user_role == "SUPER ADMIN")
                                    <li><a class="{{setActive(['company', 'company/create'])}}" href="{{url('company')}}"><span class="title">Perusahaan</span></a></li>
                                    <li><a class="{{setActive(['project', 'project/create'])}}" href="{{url('project')}}"><span class="title">Projek</span></a></li>
                                    <li><a class="{{setActive('vendor')}}" href="{{url('vendor')}}" ><span class="title">Vendor</span></a></li>
                                    @elseif (Auth::user()->user_role == "ADMIN")
                                    <li><a class="{{setActive(['category', 'category/create'])}}" href="{{url('category')}}"><span class="title">Kategori</span></a></li>
                                    <li><a class="{{setActive(['project', 'project/create'])}}" href="{{url('project')}}"><span class="title">Projek</span></a></li>
                                    <li><a class="{{setActive(['site', 'site/create'])}}" href="{{url('site')}}"><span class="title">Site</span></a></li>
                                    <li><a class="{{setActive('vendor')}}" href="{{url('vendor')}}" ><span class="title">Vendor</span></a></li>
                                    @endif
                                </ul><!--end /submenu -->
                            </li><!--end /menu-li -->
                            <!-- END -->
                        @else
                            <li class="{{ setActive('project') }}">
                                <a href="{{url('project')}}">
                                    <div class="gui-icon"><i class="glyphicon glyphicon-list-alt"></i></div>
                                    <span class="title">Project Bids</span>
                                </a>
                            </li><!--end /menu-li -->
                            <!-- END DASHBOARD -->
                        @endif
                    @endif
                    </ul><!--end .main-menu -->
                    <!-- END MAIN MENU -->

                    <div class="menubar-foot-panel">
                        <small class="no-linebreak hidden-folded">
                            <center><i>{{date("d M, Y")}} <span id="clock"></span></i></center>
                            <center>Centratama Group &copy; {{date('Y')}}</center>
                        </small>
                    </div>
                </div><!--end .menubar-scroll-panel-->
            </div><!--end #menubar-->
            <!-- END MENUBAR -->

            <!-- BEGIN OFFCANVAS RIGHT -->
            <!-- END OFFCANVAS RIGHT -->

        </div><!--end #base-->
        <!-- END BASE -->

        <!-- BEGIN JAVASCRIPT -->
        {!! Html::script('assets/materialadmin/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/bootstrap/bootstrap.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/spin.js/spin.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/autosize/jquery.autosize.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/moment/moment.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/flot/jquery.flot.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/flot/jquery.flot.time.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/flot/jquery.flot.resize.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/flot/jquery.flot.orderBars.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/flot/jquery.flot.pie.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/flot/curvedLines.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/jquery-knob/jquery.knob.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/sparkline/jquery.sparkline.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/d3/d3.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/d3/d3.v3.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/libs/rickshaw/rickshaw.min.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/source/App.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/source/AppNavigation.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/source/AppOffcanvas.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/source/AppCard.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/source/AppForm.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/source/AppNavSearch.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/source/AppVendor.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/demo/Demo.js') !!}
        {!! Html::script('assets/materialadmin/assets/js/core/demo/DemoDashboard.js') !!}
        <!-- END JAVASCRIPT -->

        @include('sweet::alert')

        <script type="text/javascript">
            <?php
                $today = getdate();
            ?>
            var d = new Date("{{date('Y-m-d H:i:s')}}");
            setInterval(function() {
                d.setSeconds(d.getSeconds() + 1);
                $('#clock').text((('0' + d.getHours()).slice(-2) +':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2) ));
            }, 1000);
        </script>
    </body>
</html>
