<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Laporan Project</title>
    <body>
      <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-3wr7{font-weight:bold;font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
      </style>
  
      <div style="font-family:Arial; font-size:12px;">
        <center><h2>Data Project</h2></center>  
      </div>
      <br>
      <table class="tg">
        <tr>
          <th class="tg-3wr7" width="20%">Nama Projek:</th>
          <td class="tg-rv4w">{{$data['project'][0]->project_name}}</td>
          <th class="tg-3wr7" width="20%">Diminta Oleh:</th>
          <td class="tg-rv4w">{{$data['project'][0]->company_name}}</td>
        </tr>
        <tr>
          <th class="tg-3wr7" width="20%">Jadwal Projek:</th>
          <td class="tg-rv4w" colspan="3">{{date("d M, Y", strtotime($data['project'][0]->project_start_date))}} - {{date("d M, Y", strtotime($data['project'][0]->project_end_date))}}</td>
        </tr>
        <tr>
          <th class="tg-3wr7" width="20%">Jadwal Penawaran:</th>
          <td class="tg-rv4w" colspan="3">{{date("d M, Y h:i A", strtotime($data['project'][0]->biding_start))}} - {{date("d M, Y h:i A", strtotime($data['project'][0]->biding_finish))}}</td>
        </tr>
        <tr>
          <th class="tg-3wr7" width="20%">Deskripsi:</th>
          <td class="tg-rv4w" colspan="3">{{$data['project'][0]->desc}}</td>
        </tr>
      </table>
      <br>

      <div style="font-family: Arial; font-size: 12px;">
        <center><h2>Daftar Peringkat Peserta</h2></center>
      </div>
      <table class="tg">
        <tr>
          <th class="tg-3wr7" width="5%">#</th>
          <th class="tg-3wr7" width="30%">Vendor</th>
          <th class="tg-3wr7">Surel</th>
          <th class="tg-3wr7">Penawaran Terakhir</th>
        </tr>
        @php ($i = 0)
        @foreach($data['pv'] as $key => $pv)
            <tr>
              <th class="tg-3wr7">{{$pv->rank}}.</th>
              <td class="tg-rv4w">{{$pv->vendor_name}}</td>
              <td class="tg-rv4w">{{$pv->vendor_email}}</td>
              <td class="tg-rv4w">{{number_format($pv->prices_bid, 2, ',', '.')}}</td>
            </tr>
        @endforeach
      </table>
      <br>

      <div style="font-family: Arial; font-size: 12px;">
        <center>
          Penawaran Terbaik dari <b>{{$data['best_price'][0]->vendor_name}}</b> dengan nominal
          <b>{{number_format($data['best_price'][0]->prices_bid, 2, ',', '.')}}</b>
        </center>
      </div>

      <div style="font-family: Arial; font-size: 12px;">
        <center><h2>History Penawaran</h2></center>
      </div>
      @foreach($data['history'] as $key => $history)
        @if(!empty($history))
        <div>{{$history[0]->vendor_name}}</div>
        <table class="tg">
          <tr>
            <th class="tg-3wr7" width="5%">#</th>
            <th class="tg-3wr7" width="30%">Harga</th>
            <th class="tg-3wr7">Tanggal</th>
          </tr>
          @foreach($history as $index => $bid)
            <tr>
              <th class="tg-3wr7">{{++$index}}.</th>
              <td class="tg-rv4w">{{number_format($bid->prices_bid, 2, ',', '.')}}</td>
              <td class="tg-rv4w">{{date("d M, Y h:i:s A", strtotime($bid->created_at))}}</td>
            </tr>
          @endforeach
        </table>
        @endif
        <br>
      @endforeach
    </body>
  </head>
</html>
