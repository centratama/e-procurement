ALTER TABLE `tproject_price_component`
	ADD COLUMN `upper_limit_price` INT(11) NULL DEFAULT NULL AFTER `component`,
	ADD COLUMN `lower_limit_price` INT(11) NULL DEFAULT NULL AFTER `upper_limit_price`,
	ADD COLUMN `multiples_price` INT(11) NULL DEFAULT NULL AFTER `lower_limit_price`;

ALTER TABLE `tproject_price_component`
	ADD COLUMN `unit` VARCHAR(50) NULL DEFAULT NULL AFTER `multiples_price`,
	ADD COLUMN `qty` INT NULL DEFAULT NULL AFTER `unit`;