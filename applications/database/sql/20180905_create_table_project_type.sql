CREATE TABLE `tmaster_project_type` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`desc` VARCHAR(100) NOT NULL,
	`validation_type` INT NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

INSERT INTO `db_eproc`.`tmaster_project_type` (`desc`, `validation_type`, `created_at`) VALUES ('Default', '1', '2018-09-05 15:08:06');
INSERT INTO `db_eproc`.`tmaster_project_type` (`desc`, `validation_type`, `created_at`) VALUES ('4 IN 1', '2', '2018-09-05 15:08:17');