<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SiteModel extends Model
{
    //
    function __construct()
    {
    	$this->table = 'tmaster_site';
    }

    function getAllData()
    {
    	return DB::table($this->table)->select('*')->get();
    }

    function getWhereId($id)
    {
        return DB::table($this->table)
                    ->join('tmaster_company', $this->table.'.company_id', '=', 'tmaster_company.id')
                    ->select($this->table.'.*', 'tmaster_company.*')
                    ->where($this->table.'.site_id', '=', $id)
                    ->get();

    }

    function insertData($post)
    {
    	return DB::table($this->table)->insertGetId($post);
    }

    function getSiteByCompany($company_id)
    {
        $data = DB::table($this->table)
                ->where('company_id', $company_id)
                ->orderBy('site_name', 'asc')
                ->lists('site_name','site_id');

        return $data;   
    }

    function updateData($post, $id)
    {
        return DB::table($this->table)
                    ->where('site_id', $id)
                    ->update($post);   
    }
}
