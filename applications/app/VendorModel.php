<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class VendorModel extends Model
{
    //
    function __construct()
	{
		$this->table = "tmaster_vendor";
	}

	function insertData($post)
	{
		$category = '';
		foreach ($post['input']['category'] as $key => $category_id) {
			$category .= ($category != '') ? ',' : '';
			$category .= $category_id;
		}
		
		$dataVendor = array(
				"vendor_type" => $post['input']['vendor_type'],
				"category_id" => $category,
				"vendor_email" => $post['input']['vendor_email'],
				"vendor_name" => $post['input']['vendor_name'],
				"city_id" => $post['input']['city'],
				"vendor_address" => $post['input']['address'],
				"zipcode" => $post['input']['zipcode'],
				"npwp" => $post['input']['npwp'],
				"created_at" => date("Y-m-d H:i:s"),
			);

		return DB::table($this->table)->insertGetId($dataVendor);
	}

	function count()
	{
		$data = DB::table($this->table)->count();

		return $data;
	}

	function getById($id)
	{
		$data = DB::table($this->table)->where('id', $id)->get();

		return $data;
	}

	function getAllData()
	{
		$data = DB::table($this->table)
					->join('tmaster_city', $this->table.'.city_id', '=', 'tmaster_city.id')
					->join('tmaster_province', 'tmaster_city.province_id', '=', 'tmaster_province.id')
					->select($this->table.'.*', 'tmaster_city.city_name', 'tmaster_province.province_name')
					->where($this->table.'.is_active', '<>', '0')
    				->get();

    	return $data;
	}

	function getWhereId($id)
	{
		$data = DB::table($this->table)
					->join('tmaster_city', $this->table.'.city_id', '=', 'tmaster_city.id')
					->join('tmaster_province', 'tmaster_city.province_id', '=', 'tmaster_province.id')
					->select($this->table.'.*', 'tmaster_city.city_name', 'tmaster_city.id as city_id', 'tmaster_city.province_id', 'tmaster_province.province_name')
					->where($this->table.'.id', '=', $id)
    				->get();

    	return $data;
	}

	function getByCategory($category_id)
	{
		$cat_ids = explode(",", $category_id);
		$catid = "";
		foreach ($cat_ids as $key => $cat_id) {
			$catid .= ($catid != '') ? '|' : '';
			$catid .= $cat_id;
		}
		$sql = "SELECT * FROM {$this->table} WHERE CONCAT(',', category_id, ',') REGEXP ',({$catid}),';";
		
		$data = DB::SELECT($sql);

		return $data;
	}
	
	function ActivateVendor($id)
	{
		$sql = "UPDATE {$this->table} SET is_active = '1' WHERE id = '{$id}';";

		DB::UPDATE($sql);

		return true;
	}

	function updateData($post, $id)
	{
		$category = '';
		foreach ($post['input']['category'] as $key => $category_id) {
			$category .= ($category != '') ? ',' : '';
			$category .= $category_id;
		}

		$dataVendor = array(
				"vendor_type" => $post['input']['vendor_type'],
				"category_id" => $category,
				"vendor_email" => $post['input']['vendor_email'],
				"vendor_name" => $post['input']['vendor_name'],
				"city_id" => $post['input']['city'],
				"vendor_address" => $post['input']['address'],
				"zipcode" => $post['input']['zipcode'],
				"npwp" => $post['input']['npwp'],
				"updated_at" => date("Y-m-d H:i:s"),
			);

		return DB::table($this->table)
					->where('id', $id)
                    ->update($dataVendor);
	}

	function updateStatus($post, $id)
	{
		return DB::table($this->table)
					->where('id', $id)
                    ->update($post);	
	}
}
