<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome')->with('success', 'Horaay!!');
});

Route::auth();

Route::get('/home', 'HomeController@index');

// Vendor URL attributes
Route::get('vendor/regist', 'VendorController@create');
Route::post('vendor/store-vendor', 'VendorController@store');
Route::get('vendor', 'VendorController@index')->middleware('auth');
Route::get('vendor/view/{id}', 'VendorController@show')->middleware('auth');
Route::get('vendor/edit/{id}', 'VendorController@edit')->middleware('auth');
Route::patch('vendor/update/{id}', array('uses' => 'VendorController@update', 'as' => 'update-vendor'))->middleware('auth');
Route::patch('/vendor/update-status/{id}', array('as' => 'update-status', 'uses' => 'VendorController@updateStatus'));

// Province City
Route::get('city-province/{province_id}', 'ProvinceCityController@getCityByProvince');

// Verify Account
Route::get('verify', 'VendorController@verifyAccount');

// Company
Route::get('company', 'CompanyController@index');
Route::get('company/create', 'CompanyController@create');
Route::post('company/store-company', 'CompanyController@store');
Route::get('company/view/{id}', 'CompanyController@show');

// Category
Route::get('category', 'CategoryController@index');
Route::get('category/create', 'CategoryController@create');
Route::post('category/store-category', 'CategoryController@store');
Route::get('category/view/{id}', 'CategoryController@show');
Route::get('category/edit/{id}', 'CategoryController@edit');
Route::patch('category/update/{id}', array('uses' => 'CategoryController@update', 'as' => 'update-category'));

// Project
Route::get('project', 'ProjectController@index');
Route::get('project/create', 'ProjectController@create');
Route::post('project/store-project', 'ProjectController@store');
Route::get('project/view/{id}', 'ProjectController@show');
Route::get('project/edit/{id}', 'ProjectController@edit');
Route::patch('project/update/{id}', array('uses' => 'ProjectController@update', 'as' => 'update-project'));
Route::get('project/publish/{id}', 'ProjectController@publish');
Route::post('project/close', 'ProjectController@close');
Route::post('project/mailToProc', 'ProjectController@mailToProc');
Route::post('project/startBid', 'ProjectController@startBid');
Route::get('project/generatePdf/{id}', 'ProjectController@generatePdf');
Route::get('project/need-review/{id}', 'ProjectController@needReview');
Route::get('project/accept-or-revise', 'ProjectController@acceptOrRevise');
Route::post('project/store-price-comp', 'ProjectController@storePriceComp');
Route::post('project/update-price-range', 'ProjectController@updatePriceRange');

// Site
Route::get('site', 'SiteController@index');
Route::get('site/create', 'SiteController@create');
Route::post('site/store-site', 'SiteController@store');
Route::get('site/view/{id}', 'SiteController@show');
Route::get('site/edit/{id}', 'SiteController@edit');
Route::patch('site/update/{id}', array('uses' => 'SiteController@update', 'as' => 'update-site'));
Route::get('site/site-company/{company_id}', 'SiteController@getSiteByCompany');

// Project-Vendor
Route::get('project-vendor/reject/{id}', 'ProjectVendorController@reject');
Route::get('project-vendor/accept/{id}', 'ProjectVendorController@accept');
Route::get('show-project', 'ProjectController@showProject');
Route::post('project-vendor/winner', 'ProjectVendorController@postWinner');
Route::post('project-vendor/upload', 'ProjectVendorController@upload');
Route::post('project-vendor/confirm', 'ProjectVendorController@confirm');

// Project-bids
Route::post('project-bids/save', 'BiddingController@store');
Route::post('project-bids/revise', 'BiddingController@revise');

// User
Route::get('password/change', 'UsersController@changePass');
Route::post('password/store-password', 'UsersController@storePass');

// News
Route::get('news', function(){
	return view('components.news.index');
});

// Registration Book
Route::get('manual-book', function(){
	return view('components.manual-book-registration');
});

// Blacklist Vendor
Route::get('vendor-blacklist', function(){
	return view('components.vendor-blacklist');
});

// Rules
Route::get('rules', function(){
	return view('components.rules.index');
});