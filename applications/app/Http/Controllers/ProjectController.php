<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProjectModel;
use App\CompanyModel;
use App\CategoryModel;
use App\VendorModel;
use App\ProjectVendorModel;
use App\BiddingModel;
use App\SysLookupValuesModel;
use App\SiteModel;
use App\PriceComponentModel;
use Validator;
use Alert;
use Auth;
use Redirect;
use Crypt;
use File;
use PDF;
use DB;

class ProjectController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->ProjectModel = new ProjectModel;
        $this->CompanyModel = new CompanyModel;
        $this->CategoryModel = new CategoryModel;
        $this->VendorModel = new VendorModel;
        $this->ProjectVendorModel = new ProjectVendorModel;
        $this->BiddingModel = new BiddingModel;
        $this->SysLookupValuesModel = new SysLookupValuesModel;
        $this->SiteModel = new SiteModel;
        $this->PriceComponentModel = new PriceComponentModel;
        $this->dir = $_SERVER['DOCUMENT_ROOT']."/attachments/document/";
        // $this->dir = $_SERVER['DOCUMENT_ROOT']."/cg/_privacy/e-procurement/attachments/document/";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->user_vendor != NULL) {
            $user_vendor = Auth::user()->user_vendor;
            $data['vendor'] = $this->VendorModel->getById($user_vendor);
            $category_id = $data['vendor'][0]->category_id;
            $cat_ids = explode(",", $category_id);
            $catid = "";
            foreach ($cat_ids as $key => $cat_id) {
                $catid .= ($catid != '') ? '|' : '';
                $catid .= $cat_id;
            }

            $data['project'] = $this->ProjectModel->getProjectByVendorCategory($user_vendor, $catid);
            $i = 0;
            foreach ($data['project'] as $key => $project) {
                $category = explode(",", $project->category_id);
            
                $cat_name = '';
                foreach ($category as $index => $cat_id) {
                    $getCategory = $this->CategoryModel->getWhereId($cat_id);
                    $cat_name .= ($cat_name != '') ? ' ' : '';
                    $cat_name .= "<span class='label label-primary'>".$getCategory[0]->category_name."</span>";
                }
            $data['category'][$i] = $cat_name;
            $data['crypt'][$i] = Crypt::encrypt($project->id);
            $i++;
            }

            return view('components.vendor_bids.index', compact('data'));

        } else {
            $data['project'] = $this->ProjectModel->getAllData();
            if (Auth::user()->user_id == "BOD") {
                $data['project'] = $this->ProjectModel->getAllDataBOD();
            }
            $i = 0;
            foreach ($data['project'] as $key => $project) {
                $category = explode(",", $project->category_id);
            
                $cat_name = '';
                foreach ($category as $index => $cat_id) {
                    $getCategory = $this->CategoryModel->getWhereId($cat_id);
                    $cat_name .= ($cat_name != '') ? ' ' : '';
                    $cat_name .= "<span class='label label-primary'>".$getCategory[0]->category_name."</span>";
                }
            $data['category'][$i] = $cat_name;
            $i++;
            }

            return view('components.project.index', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['company'] = $this->CompanyModel->getAllData();
        $data['category'] = $this->CategoryModel->getAllData();
        $data['project_type'] = $this->ProjectModel->getAllProjectType();

        return view('components.project.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
                'company' => 'required|not_in:-- Silakan Pilih --',
                'site' => 'max:255',
                'project_name' => 'required|max:255',
                'category' => 'required|not_in:-- Silakan Pilih --',
                'description' => 'required|max:255',
                'description_of_place' => 'required|max:255',
                'project_range' => 'required',
                'biding_range' => 'required',
                'attachment.*' => 'mimes:jpeg,png,pdf,jpg',
                // 'price_component' => 'required',
                'project_type' => 'required|not_in:-- Silakan Pilih --'
            ];
        // if (Auth::user()->user_role == "SUPER ADMIN") {
        //     $rules['price_lower_limit'] = 'required|numeric';
        //     $rules['price_upper_limit'] = 'required|numeric';
        //     $rules['multiples_value'] = 'required|numeric';
        // }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('project/create')
                        ->withErrors($validator)
                        ->withInput();
        } else {                        
            $project_range = $this->separateDateRange($request->input('project_range'), 'date');
            $biding_range = $this->separateDateRange($request->input('biding_range'), 'datetime');

            // Attachments
            $files = $request->file('attachment');
            if (count($files) == 1) {
                if ($files[0] == null) {
                    $allFilename = null;
                } else {
                    $filename = "Informasi - eProcurement (".$request->input('project_name').")_".date("Ymd").".".$files[0]->getClientOriginalExtension();
                    $allFilename = $filename;
                    $files[0]->move($this->dir, $filename);    
                }
            } else {
                $allFilename = "";
                foreach ($files as $key => $file) {
                    $filename = "Informasi - eProcurement (".$request->input('project_name').") ".++$key."_".date("Ymd").".".$file->getClientOriginalExtension();
                    $file->move($this->dir, $filename);   
                    $allFilename .= ($allFilename != '') ? ',' : '';
                    $allFilename .= $filename;
                }
            }
                

            $category = '';
            foreach ($request->input('category') as $key => $category_id) {
                $category .= ($category != '') ? ',' : '';
                $category .= $category_id;
            }

            $data = [
                'company_id' => $request->input('company'),
                'site' => $request->input('site'),
                'project_name' => $request->input('project_name'),
                'category_id' => $category,
                'desc' => $request->input('description'),
                'desc_place' => $request->input('description_of_place'),
                // 'price_upper_limit' => $request->input('price_upper_limit'),
                // 'price_lower_limit' => $request->input('price_lower_limit'),
                // 'multiples_value' => $request->input('multiples_value'),
                'project_start_date' => $project_range['start'],
                'project_end_date' => $project_range['finish'],
                'biding_start' => $biding_range['start'],
                'biding_finish' => $biding_range['finish'],
                'attachment' => $allFilename,
                'created_at' => date("Y-m-d h:i:s"),
                'project_type' => $request->input('project_type'),
                ];

            $lastId = $this->ProjectModel->insertData($data);

            // Insert price component
            // $this->PriceComponentModel->insertData($lastId, $request->input('price_component'));
                
            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("project/view/".$lastId);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['project'] = $this->ProjectModel->getWhereId($id);
        $data['price_comp'] = $this->PriceComponentModel->getWhereProjectId($data['project'][0]->id);
        $data['project_type'] = $this->ProjectModel->getProjectTypeById($data['project'][0]->project_type);
        $file = $this->dir.$data['project'][0]->attachment;
        if (Auth::user()->user_vendor != NULL) {
            $user_vendor = Auth::user()->user_vendor;
            $data['pv'] = $this->ProjectVendorModel->getWhereIdForVendor($id, $user_vendor);
            if (!empty($data['pv'])) {
                $data['history'] = $this->BiddingModel->getBidHistory($data['pv'][0]->id);
                $data['current_bid'] = $this->BiddingModel->getCurrentBid($data['pv'][0]->id);
                $data['count_pv'] = $this->ProjectVendorModel->getWhereProjectIdAndAccept($id);
                $data['max_bid'] = 5;
            }
            
        } else {
            $data['vendor'] = $this->ProjectVendorModel->getWhereProjectId($id);
            if (!empty($data['vendor'])) {
                foreach ($data['vendor'] as $key => $vendor) {
                    $getBidHistory[] = $this->BiddingModel->getBidHistory($vendor->tpv_id);
                }
                $data['history'] = $getBidHistory;
                $data['best_price'] = $this->BiddingModel->getBestPrice($id);
            }
            
            $data['close_type'] = $this->SysLookupValuesModel->getLookupByType('CLOSE_TYPE');
            if ($data['project'][0]->status == 2) {
                $i = 0;
                foreach ($data['vendor'] as $key => $vendor) {
                    $selected_vendor[$i] = $vendor;
                    $price_min_vendor[$i] = $this->BiddingModel->getLastPricePerVendor($vendor->tpv_id);
                    $data['selected_vendor'][$i]['vendor'] = $vendor;
                    $data['selected_vendor'][$i]['price'] = $this->BiddingModel->getLastPricePerVendor($vendor->tpv_id);
                    $i++;
                }
            }
            $data['pv'] = $this->ProjectVendorModel->getWhereProjectId($id);
            if (!empty($data['pv'])) {
                foreach ($data['pv'] as $key => $pv) {
                    $disabled = "";
                    if (in_array($pv->is_upload, array('0','2')) || $pv->status == 1) {
                        $disabled = "disabled";
                    }
                    $data['pv_disabled'][$key] = $disabled;
                }
            }
        }

        if ($data['project'][0]->status == 4) {
            $data['winner'] = $this->ProjectVendorModel->getWinner($id);
        }

        $crypt = Crypt::encrypt($id);

        if ($data['project'][0]->attachment == NULL) {
            $attach = "Tidak ada dokumen terlampir.";
        } else {
            $files = explode(",", $data['project'][0]->attachment);
            
            $attach = "<ul>";
            foreach ($files as $key => $file) {
                $attach .= "<li><a download='{$file}' href='../../attachments/document/{$file}' class='btn-link'>{$file} <i class='md md-file-download'></i></a></li>";
            }
            $attach .= "</ul>";
        }
        
        return view('components.project.show', compact('data', 'crypt', 'attach'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['project'] = $this->ProjectModel->getWhereId($id);
        $file = $this->dir.$data['project'][0]->attachment;
        $data['company'] = $this->CompanyModel->getAllData();
        $data['category'] = $this->CategoryModel->getAllData();
        $data['cat_id'] = explode(",", $data['project'][0]->category_id);
        $data['site'] = $this->SiteModel->getAllData();
        $data['project_type'] = $this->ProjectModel->getAllProjectType();
        
        // Price Component
        $price_component = $this->PriceComponentModel->getWhereProjectId($id);
        $price_comp = [];
        foreach ($price_component as $key => $value) {
            $prices_comp[$key] = $value->component;
        }
        $data['price_comp'] = implode(",", $prices_comp);
        // 

        $date['project_date'] = $this->combineDateRange($data['project'][0]->project_start_date, $data['project'][0]->project_end_date, 'date');
        $date['bidding_date'] = $this->combineDateRange($data['project'][0]->biding_start, $data['project'][0]->biding_finish, 'datetime');

        if ($data['project'][0]->attachment == NULL) {
            $attach = "Tidak ada dokumen terlampir.";
        } else {
            $files = explode(",", $data['project'][0]->attachment);
            
            $attach = "<ul>";
            foreach ($files as $key => $file) {
                $attach .= "<li><a download='{$file}' href='../../attachments/document/{$file}' class='btn-link'>{$file} <i class='md md-file-download'></i></a></li>";
            }
            $attach .= "</ul>";
        }

        if ($data['project'][0]->status != 0) {
            alert()->error("Projek tidak dapat diedit.", "Galat");

            return redirect::to('/project');
        } else {

            return view('components.project.edit', compact('data', 'date', 'attach'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
                'company' => 'required|not_in:-- Silakan Pilih --',
                'site' => 'max:255',
                'project_name' => 'required|max:255',
                'category' => 'required|not_in:-- Silakan Pilih --',
                'description' => 'required|max:255',
                'description_of_place' => 'required|max:255',
                'project_range' => 'required',
                'biding_range' => 'required',
                'attachment.*' => 'mimes:jpeg,png,pdf',
                // 'price_component' => 'required'
            ];
        // if (Auth::user()->user_role == "SUPER ADMIN") {
        //     $rules['price_lower_limit'] = 'required|numeric';
        //     $rules['price_upper_limit'] = 'required|numeric';
        //     $rules['multiples_value'] = 'required|numeric';
        // }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('project/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $project_range = $this->separateDateRange($request->input('project_range'), 'date');
            $biding_range = $this->separateDateRange($request->input('biding_range'), 'datetime');
            $category = '';
            foreach ($request->input('category') as $key => $category_id) {
                $category .= ($category != '') ? ',' : '';
                $category .= $category_id;
            }

            $data = [
                'company_id' => $request->input('company'),
                'site' => $request->input('site'),
                'project_name' => $request->input('project_name'),
                'category_id' => $category,
                'desc' => $request->input('description'),
                'desc_place' => $request->input('description_of_place'),
                // 'price_upper_limit' => $request->input('price_upper_limit'),
                // 'price_lower_limit' => $request->input('price_lower_limit'),
                // 'multiples_value' => $request->input('multiples_value'),
                'project_start_date' => $project_range['start'],
                'project_end_date' => $project_range['finish'],
                'biding_start' => $biding_range['start'],
                'biding_finish' => $biding_range['finish'],
                'updated_at' => date("Y-m-d h:i:s"),
                'project_type' => $request->input('project_type'),
            ];

            // Attachments
            // Attachments
            $files = $request->file('attachment');
            if (count($files) == 1) {
                if ($files[0] == null) {
                    // $allFilename = null;
                } else {
                    File::delete($this->dir.$request->input('filename'));
                    $filename = "Informasi - eProcurement (".$request->input('project_name').")_".date("Ymd").".".$files[0]->getClientOriginalExtension();
                    $allFilename = $filename;
                    $files[0]->move($this->dir, $filename);
                    $data['attachment'] = $allFilename;    
                }
            } else {
                $oldFiles = explode(",", $request->input('filename'));
                foreach ($oldFiles as $key => $oldFile) {
                    File::delete($this->dir.$oldFile);
                }

                $allFilename = "";
                foreach ($files as $key => $file) {
                    $filename = "Informasi - eProcurement (".$request->input('project_name').") ".++$key."_".date("Ymd").".".$file->getClientOriginalExtension();
                    $file->move($this->dir, $filename);   
                    $allFilename .= ($allFilename != '') ? ',' : '';
                    $allFilename .= $filename;
                }
                $data['attachment'] = $allFilename;
            }

            $updated = $this->ProjectModel->updateData($data, $id);

            // Delete Price Component then Insert again
            // $this->PriceComponentModel->deleteData($id);
            // $this->PriceComponentModel->insertData($id, $request->input('price_component'));
            // 
            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("project/view/".$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Separate Date Range by Type field(datetime, date).
     *
     * @param  date  $daterange
     * @param  type  $type
     * @return $separateDate
     */
    private function separateDateRange($daterange, $type)
    {
        $range = explode("-", $daterange);
        $start = trim($range[0]);
        $finish = trim($range[1]);

        switch ($type) {
            case 'datetime':
                $hourStart = date("H:i:s", strtotime(substr($start, 11, 8)));
                $dateStart = date("Y-m-d", strtotime(substr($start, 1, 10)));
                $hourFinish = date("H:i:s", strtotime(substr($finish, 11, 8)));
                $dateFinish = date("Y-m-d", strtotime(substr($finish, 1, 10)));
                
                $startDate = implode(" ", array($dateStart, $hourStart));
                $finishDate = implode(" ", array($dateFinish, $hourFinish));
                $date['start'] = $startDate;
                $date['finish'] = $finishDate;
                break;
            
            default:
                $startDate = date_format(date_create($start), "Y-m-d");
                $finishDate = date_format(date_create($finish), "Y-m-d");
                $date['start'] = $startDate;
                $date['finish'] = $finishDate;
                break;
        }
        
        return $date;
    }

    public function publish($id)
    {
        ini_set('max_execution_time', 300);
        $getProject = $this->ProjectModel->getWhereId($id);
        if ($getProject[0]->price_lower_limit == NULL && $getProject[0]->price_upper_limit == NULL) {
            alert()->error("Harga batas atas dan batas bawah belum ditentukan", "Galat")->autoclose(4500);

            return redirect('project/edit/'.$id);

        } else {
            $now = date("Y-m-d H:i:s");
            if ($getProject[0]->biding_start < $now) {
                alert()->error("Jadwal penawaran sudah terlewat, silakan ubah terlebih dahulu.", "Galat")->autoclose(4500);

                return redirect('project/edit/'.$id);
            } else {
                $this->ProjectModel->publishProject($id);
                $attachment = $_SERVER['DOCUMENT_ROOT']."/attachments/FORM SURAT PERNYATAAN PERSETUJUAAN E PROCUREMENT.docx";
                // $attachment = $_SERVER['DOCUMENT_ROOT']."/_bitbucket/e-procurement/attachments/FORM SURAT PERNYATAAN PERSETUJUAAN E PROCUREMENT.docx";
                $layout = 'publish_project';
                $link = url('/')."/show-project?id=".Crypt::encrypt($getProject[0]->id);

                $attrMail = [
                    'subject' => 'Pemberitahuan Project Terbaru',
                    'site' => $getProject[0]->site,
                    'company' => $getProject[0]->company_name,
                    'project_name' => $getProject[0]->project_name,
                    'project_start_date' => $getProject[0]->project_start_date,
                    'project_end_date' => $getProject[0]->project_end_date,
                    'biding_start' => $getProject[0]->biding_start,
                    'biding_finish' => $getProject[0]->biding_finish,
                    'desc' => $getProject[0]->desc,
                    'link' => $link,
                ];

                if ($getProject[0]->attachment != null) {
                    $files = explode(",", $getProject[0]->attachment);
                    foreach ($files as $key => $file) {
                        $attrMail['attachment'][$key] = $this->dir.$file;
                    }
                }

                // Get Data Vendor By Category and is_active = 1 to send an email about project
                $getVendor = $this->VendorModel->getByCategory($getProject[0]->category_id);

                foreach ($getVendor as $key => $vendor) {
                    $now = date("Y-m-d h:i:s");
                    // $due_date = date("Y-m-d h:i:s", strtotime("+2 day", $now));
                    $data = [
                        'project_id' => $id,
                        'vendor_id' => $vendor->id,
                        'status' => '0',
                        'created_at' => $now,
                        'invitation_due_date' => date("Y-m-d H:i:s", strtotime("{$now} +2 day"))
                    ];

                    $lastId = $this->ProjectVendorModel->insertData($data);

                    $attrMail['to'] = $vendor->vendor_email;
                    $attrMail['name'] = $vendor->vendor_name;
                    $attrMail['form'] = $attachment;
                    sendMail($attrMail, $layout);
                }
                alert()->success("Projek telah berhasil ter-publish.", "Sukses")->autoclose(4500);

                return Redirect::to('project');
            }
        }
    }

    public function showProject()
    {
        $id = \Request::get('id');
        $project_id = Crypt::decrypt($id);

        return $this->show($project_id);
    }

    public function close(Request $request)
    {
        if ($request->ajax()) {
	    $project_id = $request->input('project_id');
            $getLookup = $this->SysLookupValuesModel->getLookupByTypeValue('CLOSE_TYPE', $request->input('close_type'));
	    $getProject = $this->ProjectModel->getWhereId($project_id);
	    $getPV = $this->ProjectVendorModel->getWhereProjectIdAndAccept($project_id);
            $data = [
                'status' => $request->input('close_type')+1,
                'reason' => $getLookup[0]->lookup_desc.' - '.$request->input('reason'),
            ];

            if ($request->input('close_type') == 1) {
                # close project
            } else {
                # cancel project
		$mailLayout = 'cancel-project';
		$attrMail['subject'] = "Projek dibatalkan - ".$getProject[0]->project_name; 
		foreach ($getPV as $key => $pv){
                	if ($pv->status == 1) {
			    $attrMail['project'] = $getProject;
			    $attrMail['vendor'] = $pv;
			    $attrMail['to'] = $pv->vendor_email;
			    $attrMail['reason'] = $request->input('reason');

			    sendMail($attrMail, $mailLayout);	
			}
		}
            }
            $close = $this->ProjectModel->updateData($data, $project_id);

            return $project_id;
        }
    }

    public function mailToProc(Request $request)
    {
        if ($request->ajax()) {
            $project_id = $request->input('id');
            $post = $request->all();
            $close = $this->ProjectModel->updateData($post, $project_id);

            $getProject = $this->ProjectModel->getWhereId($project_id);
            $attr['price_comp'] = $this->PriceComponentModel->getWhereProjectId($project_id);
            
            $pdfFile = $this->generatePdf($project_id);

            $attr['pv'] = $this->ProjectVendorModel->getWhereProjectIdAndAccept($project_id);
            $attr['count_pv'] = count($attr['pv']);
            
            $link = url('/')."/project/view/".$getProject[0]->id;
            if ($getProject[0]->send_to_internal == 0) {
                $attrMail['to'] = array(
                                    'amar.fadhillah@centratamagroup.com',
                                    'setyo.wibowo@centratamagroup.com',
                                    // 'iwan.hernawan@centratamagroup.com'
                                    );
                $attrMail['subject'] = 'Proses Bidding '.$getProject[0]->project_name." Telah Selesai";
                $attrMail['project'] = $getProject;
                $attrMail['attachment'][0] = $pdfFile;
                $layout = 'close-project';
                sendMail($attrMail, $layout);
                
                $data = [
                    'send_to_internal' => 1,
                ];

                $this->ProjectModel->updateData($data, $project_id);
            }

            foreach ($attr['pv'] as $key => $pv) {
                if ($pv->status == 1) {
                    $attr['to'] = $pv->vendor_email;
                    $attr['subject'] = 'Proses Bidding '.$getProject[0]->project_name." Telah Selesai";
                    $attr['project'] = $getProject;
                    $attr['vendor'] = $pv;
                    $attr['last_price'] = $this->BiddingModel->getLastPricePerVendor($pv->tpv_id);
                    $attr['last_price_comp'] = explode(",", $attr['last_price'][0]->prices_comp);
                    // Jika menang
                    if ($pv->is_winner == 1) {
                        # code...
                        $mailLayout = 'vendor_win';
                    // Jika kalah
                    } else {
                        $mailLayout = 'vendor_lose';
                    }
                    sendMail($attr, $mailLayout);
                }
            }

            return $project_id;
        }
    }

    /**
     * Combine Date Range by Type field(datetime, date).
     *
     * @param  date  $daterange
     * @param  type  $type
     * @return $separateDate
     */
    private function combineDateRange($startdate, $finishdate, $type)
    {
        
        switch ($type) {
            case 'datetime':
                $dateStart = date("m/d/Y h:i A", strtotime($startdate));
                $dateFinish = date("m/d/Y h:i A", strtotime($finishdate));
                $date = implode(" - ", array($dateStart, $dateFinish));
                break;
            
            default:
                $dateStart = date("m/d/Y", strtotime($startdate));
                $dateFinish = date("m/d/Y", strtotime($finishdate));
                $date = implode(" - ", array($dateStart, $dateFinish));
                break;
        }

        return $date;
    }

    public function startBid(Request $request)
    {
        if ($request->ajax()) {
            $project_id = $request->input('project_id');
            $getProject = $this->ProjectModel->getWhereId($project_id);
            $data['status'] = 1;
            
            // Reject all by due date
            $this->ProjectVendorModel->rejectVendorByDueDate($project_id);
            //

            $getPV = $this->ProjectVendorModel->getWhereProjectIdAndAccept($project_id);
            if (empty($getPV)) {
                $attr['status'] = 3;
                $attr['reason'] = "Cancel Project - Tidak ada Vendor";
                $cancel = $this->ProjectModel->updateData($attr, $project_id);
            } else {
                $startBid = $this->ProjectModel->updateData($data, $project_id);
            }
            

            return $project_id;
        }
    }

    public function generatePdf($id)
    {
        $data['project'] = $this->ProjectModel->getWhereId($id);
        // $target = $_SERVER['DOCUMENT_ROOT']."/_privacy/e-procurement/attachments/document/final_report/";
        $target = $_SERVER['DOCUMENT_ROOT']."/attachments/document/final_report/";
        $filename = "[".$data['project'][0]->company_name." - ".$data['project'][0]->project_name."] Final Report-".$id.".pdf";
        $filepath = $target.$filename;
        
        // if (is_file($filepath)) {
        //     unlink($filepath);
        // }

        $data['pv'] = $this->ProjectVendorModel->getRankingByProject($id);
        $data['best_price'] = $this->BiddingModel->getBestPrice($id);
        $idComma = '';
        foreach ($data['pv'] as $key => $pv) {
            $idComma .= ($idComma != '') ? ',' : '';
            $idComma .= $pv->tpv_id;
            $getBidHistory[] = $this->BiddingModel->getBidHistory($pv->tpv_id);
        }
        $data['history'] = $getBidHistory;
        $getMinPrice = $this->BiddingModel->getWherePriceMinAndCreated($idComma, $data['project']);
        $winner = $this->ProjectVendorModel->updateWinner($getMinPrice[0]->tpv_id, $id);
        $getBidRanking = $this->BiddingModel->getBidRanking($idComma);
        // dd($getBidRanking);

        $pdf = PDF::loadView('pdf.project', compact('data', 'pdf'))
                    ->setPaper('a4', 'portrait');
        $link = url('/')."/attachments/document/final_report/";
        $pdf->save($filepath);
        $data = [
            'result_generated' => $link.$filename,
        ];
        $this->ProjectModel->updateData($data, $id);
        // return $pdf->stream();
        return $filepath;
    }

    /*
    Need review by BOD
    @param project_id
    */
    public function needReview($id)
    {
        $getProject = DB::table('tmaster_project')->where(['id' => $id])->first();
        $getPriceComp = DB::table('tproject_price_component')->where(['project_id' => $id])->count();
        $project_type = $this->ProjectModel->getProjectTypeById($getProject->project_type);

        if ($project_type->validation_type == 1) {
            if ($getProject->price_upper_limit == NULL) {
                
                alert()->error("Rentang harga belum diisi.", "Galat");

                return redirect("project/view/".$id);
            }
        } else {
            if ($getPriceComp == 0) {
                alert()->error("Belum ada komponen harga.", "Galat");

                return redirect("project/view/".$id);
            }
        }

        $data = [
            "status_review" => 0,
        ];

        $this->ProjectModel->updateData($data, $id);

        alert()->success("Data projek akan diriviu terlebih dahulu oleh BOD.", "Berhasil");

        return redirect("project/view/".$id);
    }

    public function acceptOrRevise(Request $request)
    {
        $project_id = \Request::get('id');
        $status_review = \Request::get('status');
        $desc = \Request::get('desc');

        $data = [
            "status_review" => $status_review,
            "review_desc" => $desc,
        ];

        $this->ProjectModel->updateData($data, $project_id);

        alert()->success("Data telah diubah.", "Berhasil");

        return redirect("project/view/".$project_id);
    }

    public function storePriceComp(Request $request)
    {
        $validation_type = $request->input('validation_type');
        $project_id = $request->input('project_id');
        $rules = [
                'component' => 'required|max:255',
                'unit' => 'required|max:50',
                'quantity' => 'required|integer',
        ];

        if ($validation_type != 1) {
            $rules['lower_limit_price'] = 'required|integer';
            $rules['upper_limit_price'] = 'required|integer';
            $rules['multiples_price'] = 'required|integer';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            alert()->error("Terjadi kesalahan.", "Galat");            
            return redirect('project/view/'.$project_id)
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $data = [
                'project_id' => $project_id,
                'component' => $request->input('component'),
                'unit' => $request->input('unit'),
                'qty' => $request->input('quantity'),
            ];

            if ($validation_type != 1) {
                $data['lower_limit_price'] = $request->input('lower_limit_price');
                $data['upper_limit_price'] = $request->input('upper_limit_price');
                $data['multiples_price'] = $request->input('multiples_price');
            }

            $this->PriceComponentModel->insert($data);

            alert()->success("Data berhasil disimpan.", "Berhasil");

            return redirect('project/view/'.$project_id);
        }

    }

    public function updatePriceRange(Request $request)
    {
        $project_id = $request->input('project_id');
        $rules = [
                'price_lower_limit' => 'required|integer',
                'price_upper_limit' => 'required|integer',
                'multiples_value' => 'required|integer',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            alert()->error("Terjadi kesalahan.", "Galat");            
            return redirect('project/view/'.$project_id)
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $data = [
                'price_lower_limit' => $request->input('price_lower_limit'),
                'price_upper_limit' => $request->input('price_upper_limit'),
                'multiples_value' => $request->input('multiples_value'),
            ];

            $this->ProjectModel->updateData($data, $project_id);

            alert()->success("Data berhasil disimpan.", "Berhasil");

            return redirect('project/view/'.$project_id);
        }
    }

}
