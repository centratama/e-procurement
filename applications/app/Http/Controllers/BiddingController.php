<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\BiddingModel;
use App\ProjectVendorModel;
use App\ProjectModel;

class BiddingController extends Controller
{
    //
    function __construct()
    {
    	$this->middleware('auth');
    	$this->BiddingModel = new BiddingModel;
    	$this->ProjectVendorModel = new ProjectVendorModel;
    	$this->ProjectModel = new ProjectModel;
    }

    public function store(Request $request)
    {
    	if ($request->ajax()) {
            $prices_comp = implode(",", $request->input('prices_comp'));
            $last_bid = $request->input('last_bid');
            $prices_bid = array_sum($request->input('prices_comp'));

    		$data = [
    			'prices_bid' => $prices_bid,
                'prices_comp' => $prices_comp,
    			'tpv_id' => $request->input('tpv_id'),
    			'created_at' => date("Y-m-d H:i:s"),
    		];


            $allId = $this->ProjectVendorModel->getWhereProjectIdAndAccept($request->input('project_id'));
            $getProject = $this->ProjectModel->getWhereId($request->input('project_id'));
            $id = "";
            foreach ($allId as $key => $value) {
                $id .= ($id != '') ? ',' : '';
                $id .= $value->id;
            }

            $rowData = $this->BiddingModel->checkRowData($request->input('tpv_id'));
            if($getProject[0]->status == 1) {
                if (!empty($rowData)) {
                    $isHigher = $this->BiddingModel->checkLastPrice($data);

                    if ($isHigher == true) {
                        $isAboveMinPrice = $this->BiddingModel->isAboveMinPrice($data['tpv_id'], $getProject[0]->price_lower_limit);

                        if ($isAboveMinPrice == true) {
                            alert()->error("Harga tidak boleh lebih atau sama dengan harga yang sebelumnya", "Galat")->autoclose(4500);
                        
                            return $data;
                        } else {
                            // Check if last_bid exist
                            if (!empty($last_bid)) {
                                $margin = $last_bid - $prices_bid;
                                if ($margin % $getProject[0]->multiples_value == 0) {
                                    // alert()->success("Sesuai ".$margin, "Berhasil")->autoclose(4500);
                                    $lastId = $this->BiddingModel->insertData($data);
                                    $updateStatuBid = $this->BiddingModel->updateStatus($id, $getProject);

                                    // return $lastId;
                                } else {
                                    alert()->error("Total Harga yang diajukan tidak sesuai dengan ketentuan kelipatan harga", "Galat")->autoclose(4500);
                                }
                            }    
                        }
                    } else {

                        // Check if price is lower than min price
                        if ($data['prices_bid'] < $getProject[0]->price_lower_limit) {
                            alert()->error("Harga tidak termasuk dalam rentang harga.", "Galat")->autoclose(4500);
                        
                            return $data;
                        } else {
                            if (!empty($last_bid)) {
                                $margin = $last_bid - $prices_bid;
                                if ($margin % $getProject[0]->multiples_value == 0) {
                                    // alert()->success("Sesuai ".$margin, "Berhasil")->autoclose(4500);
                                    $lastId = $this->BiddingModel->insertData($data);
                                    $updateStatuBid = $this->BiddingModel->updateStatus($id, $getProject);

                                    // return $lastId;
                                } else {
                                    alert()->error("Total Harga yang diajukan tidak sesuai dengan ketentuan kelipatan harga", "Galat")->autoclose(4500);
                                }
                            }
                        }

                    }
                } else {
                    if ($prices_bid < $getProject[0]->price_lower_limit || $prices_bid > $getProject[0]->price_upper_limit) {
                        alert()->error("Harga tidak termasuk dalam rentang harga.", "Galat")->autoclose(4500);
                    } else {
                        $margin = $getProject[0]->price_upper_limit - $prices_bid;
                        // dd($margin);
                        // IF margin = 0
                        if ($margin == 0) {
                            $lastId = $this->BiddingModel->insertData($data);
                            $updateStatuBid = $this->BiddingModel->updateStatus($id, $getProject);
                        } else {
                            if ($margin % $getProject[0]->multiples_value == 0) {
                                // alert()->success("Sesuai ".$margin, "Berhasil")->autoclose(4500);
                                $lastId = $this->BiddingModel->insertData($data);
                                $updateStatuBid = $this->BiddingModel->updateStatus($id, $getProject);

                                // return $lastId;
                            } else {
                                alert()->error("Total Harga yang diajukan tidak sesuai dengan ketentuan kelipatan harga", "Galat")->autoclose(4500);
                            }
                        }
                    }
                }
            } else {
                alert()->error("Proses bidding telah selesai", "Galat")->autoclose(4500);
            }
    	}
    }

    public function revise(Request $request)
    {
        if ($request->ajax()) {
            $project_id = $request->input('project_id');
            $prices_comp = implode(",", $request->input('prices_comp'));
            $getProject = $this->ProjectModel->getWhereId($project_id);
            $current_bid = $request->input('current_bid');
            $prices_bid = array_sum($request->input('prices_comp'));

            $data = [
                'prices_bid' => $prices_bid,
                'prices_comp' => $prices_comp,
            ];

            $id_bid = $request->input('id');

            $allId = $this->ProjectVendorModel->getWhereProjectIdAndAccept($project_id);
            $id_in = "";
            foreach ($allId as $key => $value) {
                $id_in .= ($id_in != '') ? ',' : '';
                $id_in .= $value->id;
            }

            if (array_sum($request->input('prices_comp')) > $getProject[0]->price_upper_limit) {
                alert()->error("Total harga dibawah batas bawah nilai projek", "Galat")->autoclose(4500);
            } else {
                if ($current_bid > $prices_bid) {
                    $margin = $current_bid - $prices_bid;
                    if ($margin % $getProject[0]->multiples_value == 0) {
                        $revisePricesBid = $this->BiddingModel->revisePricesBid($data, $id_bid, $id_in);
                    } else {
                        alert()->error("Total Harga yang diajukan tidak sesuai dengan ketentuan kelipatan harga", "Galat")->autoclose(4500);
                    }
                } elseif ($current_bid < $prices_bid) {
                    $margin = $prices_bid - $current_bid;
                    if ($margin % $getProject[0]->multiples_value == 0) {
                        $revisePricesBid = $this->BiddingModel->revisePricesBid($data, $id_bid, $id_in);
                    } else {
                        alert()->error("Total Harga yang diajukan tidak sesuai dengan ketentuan kelipatan harga", "Galat")->autoclose(4500);
                    }
                } else {
                    $revisePricesBid = $this->BiddingModel->revisePricesBid($data, $id_bid, $id_in);
                }
            }
        }
    }
}
