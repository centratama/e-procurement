<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProvinceCityModel;

class ProvinceCityController extends Controller
{
    //
    public function __construct()
    {
    	$this->ProvinceCityModel = new ProvinceCityModel;
    }

    public function getCityByProvince($province_id)
    {
    	$data = $this->ProvinceCityModel->getCityByProvince($province_id);

    	return response()->json($data);
    }
}
