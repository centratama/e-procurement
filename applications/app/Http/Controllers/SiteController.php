<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SiteModel;
use App\CompanyModel;
use Validator;

class SiteController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->SiteModel = new SiteModel;
        $this->CompanyModel = new CompanyModel;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['site'] = $this->SiteModel->getAllData();

        return view('components.site.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['company'] = $this->CompanyModel->getAllData();

        return view('components.site.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'site_name' => 'required|unique:tmaster_site',
            'site_id' => 'required|unique:tmaster_site',
            'company' => 'required|not_in:-- Silakan Pilih --'
        ]);
        if ($validator->fails()) {
            return redirect('site/create')
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $data = array(
                    'site_name' => $request->input('site_name'),
                    'site_id' => $request->input('site_id'),
                    'company_id' => $request->input('company'),
                    'site_address' => NULL,
                );

            $lastId = $this->SiteModel->insertData($data);

            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("site");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['site'] = $this->SiteModel->getWhereId($id);

        return view('components.site.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['site'] = $this->SiteModel->getWhereId($id);
        $data['company'] = $this->CompanyModel->getAllData();

        return view('components.site.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'site_name' => 'required',
            'site_id' => 'required',
            'company' => 'required|not_in:-- Silakan Pilih --'
        ]);
        if ($validator->fails()) {
            return redirect('site/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $data = array(
                    'site_name' => $request->input('site_name'),
                    'site_id' => $request->input('site_id'),
                    'company_id' => $request->input('company'),
                    'site_address' => NULL,
                );

            $lastId = $this->SiteModel->updateData($data, $id);

            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("site/view/".$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSiteByCompany($company_id)
    {
        $data = $this->SiteModel->getSiteByCompany($company_id);

        return response()->json($data);
    }
}
