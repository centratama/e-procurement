<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\SysLookupValuesModel;
use App\ProvinceCityModel;
use App\VendorModel;
use App\UsersModel;
use App\CategoryModel;
use Validator;
use Alert;
Use Crypt;
Use Mail;

class VendorController extends Controller
{
    //
    public function __construct()
    {
    	$this->SysLookupValuesModel = new SysLookupValuesModel;
    	$this->ProvinceCityModel = new ProvinceCityModel;
    	$this->VendorModel = new VendorModel;
        $this->UsersModel = new UsersModel;
        $this->CategoryModel = new CategoryModel;
    }

    public function index()
    {
        $data['vendor'] = $this->VendorModel->getAllData();
        $i = 0;
        foreach ($data['vendor'] as $key => $vendor) {
            $category = explode(",", $vendor->category_id);
            
            $cat_name = '';
            foreach ($category as $index => $cat_id) {
                $getCategory = $this->CategoryModel->getWhereId($cat_id);
                $cat_name .= ($cat_name != '') ? ' ' : '';
                $cat_name .= "<span class='label label-primary'>".$getCategory[0]->category_name."</span>";
            }
            $data['category'][$i] = $cat_name;
            $i++;
        }

        return view('components.vendor.index', compact('data'));
    }

    public function create()
    {
    	$data['vendor_type'] = $this->SysLookupValuesModel->getLookupByType('VENDOR_TYPE');
    	$data['vendor_from'] = $this->SysLookupValuesModel->getLookupByType('VENDOR_FROM');
    	$data['province'] = $this->ProvinceCityModel->getAllProvince();
        $data['category'] = $this->CategoryModel->getAllData();

    	return view('components.vendor.create-vendor', compact('data'));
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'vendor_type' => 'required|not_in:-- Silakan Pilih --',
            'category' => 'required|not_in:-- Silakan Pilih --',
    		'vendor_name' => 'required|min:5',
    		'vendor_email' => 'required|max:255|unique:tmaster_vendor',
    		'province' => 'required|not_in:-- Silakan Pilih --',
    		'city' => 'required|not_in:-- Silakan Pilih --,Pilih Provinsi Terlebih Dahulu',
    		'address' => 'required',
    		'npwp' => 'required',
    		'zipcode' => 'required',
    	]);

    	if ($validator->fails()) {
            return redirect('vendor/regist')
                        ->withErrors($validator)
                        ->withInput();
        } else {
        	// $vendor_name = str_replace(" ", "_", $request->input('vendor_name'));
        	// $pictNPWP = $request->file('npwp');
        	// $pictSIUP = $request->file('siup');
        	// $filenameNPWP = "NPWP-".$vendor_name.".".$pictNPWP->getClientOriginalExtension();
        	// $filenameSIUP = "SIUP-".$vendor_name.".".$pictSIUP->getClientOriginalExtension();
        	// $dirNPWP = $_SERVER['DOCUMENT_ROOT']."/_bitbucket/e-procurement/attachments/NPWP/";
        	// $dirSIUP = $_SERVER['DOCUMENT_ROOT']."/_bitbucket/e-procurement/attachments/SIUP/";

        	$data = array(
        			'input' => $request->all(),
        			// 'filenameNPWP' => $filenameNPWP,
        			// 'filenameSIUP' => $filenameSIUP,
        		);
            $lastId = $this->VendorModel->insertData($data);
            $count = $this->VendorModel->count();
            $userAccount = $this->generateUserId($count, $lastId);
        		// $pictNPWP->move($dirNPWP, $filenameNPWP);
        		// $pictSIUP->move($dirSIUP, $filenameSIUP);

        	alert()->success("Data tersimpan. Silakan Periksa Surel Anda", "Berhasil")->autoclose(4500);

        	return redirect("vendor/regist");
        }
    }

    public function show($id)
    {
        $data['vendor'] = $this->VendorModel->getWhereId($id);

        $category = explode(",", $data['vendor'][0]->category_id);
        $cat_name = '';
        foreach ($category as $index => $cat_id) {
            $getCategory = $this->CategoryModel->getWhereId($cat_id);
            $cat_name .= ($cat_name != '') ? ' ' : '';
            $cat_name .= "<span class='label label-primary'>".$getCategory[0]->category_name."</span>";
        }
        $data['category'] = $cat_name;

        return view('components.vendor.show', compact('data'));
    }

    public function generateUserId($count, $lastId)
    {
        $user_id = getUserId($count);

        // get Data Vendor By Id
        $vendor = $this->VendorModel->getById($lastId);

        // VerifyLink
        $verifyLink = url('/')."/verify?id=".Crypt::encrypt($user_id);
        
        $data = [
            "user_id" => $user_id,
            "user_vendor" => $lastId,
            "name" => $vendor[0]->vendor_name,
            "email" => $vendor[0]->vendor_email,
            "user_role" => "USER",
            "created_at" => date("Y-m-d H:i:s"),
        ];

        // Attribut Sendmail
        $attrMail = [
            "to" => $vendor[0]->vendor_email,
            "subject" => "Aktivasi Pendaftaran Vendor E-Procurement",
            "link" => $verifyLink,
            "name" => $vendor[0]->vendor_name,
        ];
        $layout = "activation";
        sendMail($attrMail, $layout);
        // End

        $userCreated = $this->UsersModel->createUser($data);

        return true;
    }

    public function verifyAccount()
    {
        $id = \Request::get('id');
        $user_id = Crypt::decrypt($id);

        // Plain Password
        $passwordGenerated = generatePassword();

        $dataUser = $this->UsersModel->getWhereByUserId($user_id);
        if ($dataUser[0]->is_active == 0) {
            $activateAcc = $this->UsersModel->ActivateAcc($user_id, $passwordGenerated);
            $activateVendor = $this->VendorModel->ActivateVendor($dataUser[0]->user_vendor);

            if ($activateAcc === true) {
                $attrMail = [
                    "to" => $dataUser[0]->email,
                    "subject" => "Informasi Akun Pengguna E-Procurement",
                    "username" => $user_id,
                    "password" => $passwordGenerated,
                    "name" => $dataUser[0]->name
                ];
                $layout = "account";
                sendMail($attrMail, $layout);

                return view('components.verify');
            }
        } else {
            
            return view('components.verify-already');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['vendor_type'] = $this->SysLookupValuesModel->getLookupByType('VENDOR_TYPE');
        $data['vendor_from'] = $this->SysLookupValuesModel->getLookupByType('VENDOR_FROM');
        $data['province'] = $this->ProvinceCityModel->getAllProvince();
        $data['city'] = $this->ProvinceCityModel->getAllCity();
        $data['category'] = $this->CategoryModel->getAllData();
        $data['vendor'] = $this->VendorModel->getWhereId($id);
        $data['cat_id'] = explode(",", $data['vendor'][0]->category_id);

        return view('components.vendor.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'vendor_type' => 'required|not_in:-- Silakan Pilih --',
            'category' => 'required|not_in:-- Silakan Pilih --',
            'vendor_name' => 'required|min:5',
            'vendor_email' => 'required|max:255',
            'province' => 'required|not_in:-- Silakan Pilih --',
            'city' => 'required|not_in:-- Silakan Pilih --,Pilih Provinsi Terlebih Dahulu',
            'address' => 'required',
            'npwp' => 'required',
            'zipcode' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('vendor/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        } else {

            $data = array(
                    'input' => $request->all(),
                );
            $updated = $this->VendorModel->updateData($data, $id);

            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("vendor/view/".$id);
        }
    }

    public function updateStatus(Request $request, $id)
    {
        $data = [
            'is_active' => $request->input('status'),
        ];
        $updated = $this->VendorModel->updateStatus($data, $id);

        alert()->success("Data tersimpan.", "Berhasil");

        return redirect("vendor/view/".$id);
    }
}
