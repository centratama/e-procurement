<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProjectVendorModel;
use App\ProjectModel;
use App\VendorModel;
use Auth;
use Redirect;
use Alert;
use Validator;
use Crypt;
use File;

class ProjectVendorController extends Controller
{
    //
    protected $user_vendor;

    function __construct()
    {
    	$this->middleware('auth');
    	$this->ProjectVendorModel = new ProjectVendorModel;
        $this->ProjectModel = new ProjectModel;
        $this->VendorModel = new VendorModel;
    	$this->user_vendor = Auth::user()->user_vendor;
        $this->dir = $_SERVER['DOCUMENT_ROOT']."/attachments/document/agreement";
        // $this->dir = $_SERVER['DOCUMENT_ROOT']."/_bitbucket/e-procurement/attachments/document/agreement";
    }

    public function reject($id)
    {
    	$data = [
    		'status' => '1',
    		'updated_at' => date("Y-m-d H:i:s"),
    	];
    	$where = [
    		'project_id' => $id,
    		'vendor_id' => $this->user_vendor,
    	];

    	$update = $this->ProjectVendorModel->updateStatus($data, $where);

    	return Redirect::to('project/view/'.$id);

    }

    public function accept($id)
    {
    	$data = [
    		'status' => '2',
    		'updated_at' => date("Y-m-d H:i:s"),
    	];
    	$where = [
    		'project_id' => $id,
    		'vendor_id' => $this->user_vendor,
    	];

    	$update = $this->ProjectVendorModel->updateStatus($data, $where);

    	return Redirect::to('project/view/'.$id);

    }

    public function postWinner(Request $request)
    {
        if ($request->ajax()) {
            $tpv_id = $request->input('tpv_id');
            $getProject = $this->ProjectVendorModel->getProjectIdByTpvId($tpv_id);
            $updateStatus = $this->ProjectVendorModel->postWinner($tpv_id, $getProject[0]->id);

            $data = [
                    'status' => 4,
                    ];
            $closeProject = $this->ProjectModel->updateData($data, $getProject[0]->id);

            return $getProject;
        }
    }

    public function upload(Request $request)
    {
        ini_set('max_execution_time', 300);
        $id = $request->input('id');
        $getPV = $this->ProjectVendorModel->getById($id);
        $getVendor = $this->VendorModel->getById($getPV[0]->vendor_id);
        $getProject = $this->ProjectModel->getWhereId($getPV[0]->project_id);
        $project_id = Crypt::encrypt($getPV[0]->project_id);

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:jpeg,png,pdf'
        ]);

        if ($validator->fails()) {
            return redirect('show-project?id='.$project_id)
                        ->withErrors($validator)
                        ->withInput();
        } else {
            
            $mailLayout = 'verify_doc';
            $attrMail = [
                'subject' => 'Dokumen persyaratan berhasil diunggah '.$getProject[0]->project_name,
                'to' => $getVendor[0]->vendor_email,
                'project' => $getProject,
                'vendor' => $getVendor
            ];

            $file = $request->file('file');
            $filename = "[Agreement Doc - ".$getVendor[0]->vendor_name."] ".$getProject[0]->project_name."_".date("Ymd").".".$file->getClientOriginalExtension();
            if ($getPV[0]->is_upload == 2) {
                File::delete($this->dir.$getPV[0]->file);
                $attrMail['subject'] = 'Dokumen revisi persyaratan berhasil diunggah '.$getProject[0]->project_name;
            }
            $file->move($this->dir, $filename);
            $data = [
                'is_upload' => 1,
                'file' => $filename
            ];

            $updated = $this->ProjectVendorModel->updateData($data, $id);
            sendMail($attrMail, $mailLayout);


            alert()->success("Berhasil terunggah.", "Berhasil");

            return redirect('show-project?id='.$project_id);
        }
    }

    public function confirm(Request $request)
    {
        if ($request->ajax()) {
            ini_set('max_execution_time', 300);
            $tpv_id = $request->input('id');
            $post = $request->all();
            $updated = $this->ProjectVendorModel->updateData($post, $tpv_id);

            $getPV = $this->ProjectVendorModel->getById($tpv_id);
            $getProject = $this->ProjectModel->getWhereId($getPV[0]->project_id);
            $getVendor = $this->VendorModel->getById($getPV[0]->vendor_id);

            $data['to'] = $getVendor[0]->vendor_email;
            $data['project'] = $getProject;
            $data['vendor'] = $getVendor;
            $data['link'] = url('/')."/show-project?id=".Crypt::encrypt($getPV[0]->project_id);
            if ($getPV[0]->status == 0 && $getPV[0]->is_upload == 2) {
                // Revise
                $data['subject'] = "Revisi - Dokumen Persyaratan untuk Projek ".$getProject[0]->project_name;
                $layout = "doc_revise";
                sendMail($data, $layout);
            } elseif ($getPV[0]->status == 1 && $getPV[0]->is_upload == 1) {
                // Accept
                $data['subject'] = "Terverifikasi - Dokumen Persyaratan untuk Projek ".$getProject[0]->project_name;
                $layout = "doc_accept";
                sendMail($data, $layout);
            }

            if ($updated) {
                return array('status' => 200);
            }

        }
    }

    public static function rejectAllVendorByDueDate()
    {
        $getAllProject = $this->ProjectModel->getAllData();

        foreach ($getAllProject as $key => $project) {
            $this->rejectVendorByDueDate($project->id);
        }
    }

    public function rejectVendorByDueDate($project_id)
    {
        $this->ProjectVendorModel->rejectVendorByDueDate($project_id);
    }
}
