<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\CompanyModel;

class CompanyController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->CompanyModel = new CompanyModel;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['company'] = $this->CompanyModel->getAllData();
        return view('components.company.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'remarks' => 'max:255',
        ]);
        if ($validator->fails()) {
            return redirect('company/create')
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $data = array(
                    'company_name' => $request->input('company_name'),
                    'remarks' => $request->input('remarks'),
                    'created_at' => date("Y-m-d h:i:s"),
                );

            $lastId = $this->CompanyModel->insertData($data);

            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("company");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['company'] = $this->CompanyModel->getWhereId($id);

        return view('components.company.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
