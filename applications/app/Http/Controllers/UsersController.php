<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersModel;
use Auth;
use App\Http\Requests;
use Validator;
use Redirect;

class UsersController extends Controller
{
    //
    protected $user_id;

    function __construct()
    {
    	$this->middleware('auth');
    	$this->user_id = Auth::user()->id;
    	$this->UsersModel = new UsersModel;
    }

    public function changePass()
    {
    	$data['users'] = $this->UsersModel->getWhereId($this->user_id);

    	return view('components.change-password', compact('data'));
    }

    public function storePass(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    			'password' => 'required|min:6|confirmed',
    	]);

    	if ($validator->fails()) {
            return redirect('password/change')
                        ->withErrors($validator)
                        ->withInput();
        } else {
        	$data = [
        		'password' => bcrypt($request->input('password')),
        	];

        	$this->UsersModel->updateWhereId($data, $request->input('id'));

        	alert()->success("Sandi berhasil diubah.", "Berhasil");

        	return redirect::to('password/change');
        }
    }
}
