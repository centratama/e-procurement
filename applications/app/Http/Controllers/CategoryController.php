<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\CategoryModel;

class CategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
        $this->CategoryModel = new CategoryModel;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['category'] = $this->CategoryModel->getAllData();
        return view('components.category.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('components.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required',
            'remarks' => 'max:255',
        ]);
        if ($validator->fails()) {
            return redirect('category/create')
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $data = array(
                    'category_name' => $request->input('category_name'),
                    'remarks' => $request->input('remarks'),
                    'created_at' => date("Y-m-d h:i:s"),
                );

            $lastId = $this->CategoryModel->insertData($data);

            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("category");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['category'] = $this->CategoryModel->getWhereId($id);

        return view('components.category.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category'] = $this->CategoryModel->getWhereId($id);

        return view('components.category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required',
            'remarks' => 'max:255',
        ]);
        if ($validator->fails()) {
            return redirect('category/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $data = array(
                    'category_name' => $request->input('category_name'),
                    'remarks' => $request->input('remarks'),
                    'updated_at' => date("Y-m-d h:i:s"),
                );

            $updated = $this->CategoryModel->updateData($data, $id);

            alert()->success("Data tersimpan.", "Berhasil");

            return redirect("category/view/".$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
