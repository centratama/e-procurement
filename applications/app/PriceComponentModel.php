<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PriceComponentModel extends Model
{
    //
    function __construct()
    {
    	$this->table = "tproject_price_component";
    }

    function insertData($project_id, $post)
    {
    	$components = explode(",", $post);
    	$data = [
    		'project_id' => $project_id,
    	];

    	foreach ($components as $key => $component) {
    		$data['component'] = $component;
    		DB::table($this->table)->insert($data);
    	}
    }

    function getWhereProjectId($project_id)
    {
    	$data = DB::table($this->table)
    				->select($this->table.'.*')
    				->where($this->table.'.project_id', '=', $project_id)
    				->get();

    	return $data;
    }

    function deleteData($project_id)
    {
        return DB::table($this->table)
                ->where('project_id', '=', $project_id)
                ->delete();
    }

    function insert($post)
    {
        DB::table($this->table)->insert($post);
    }
}
