<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class BiddingModel extends Model
{
    //
    function __construct()
    {
    	$this->table = "tpv_biding";
    }

    function insertData($post)
    {
        return DB::table($this->table)->insertGetId($post);
    }

    function updateStatus($idComma, $project)
    {
        $getMinPrice = $this->getWherePriceMinAndCreated($idComma, $project);
        $this->updateWhereMatch($idComma, $getMinPrice);
        $this->updateWhereNotMatch($idComma, $getMinPrice);
        $this->updateRank($idComma, $project);

    	return $getMinPrice;
    }

    function getBidHistory($id)
    {
    	return DB::table($this->table)
                    ->join('tproject_vendor', $this->table.'.tpv_id', '=', 'tproject_vendor.id')
                    ->join('tmaster_vendor', 'tproject_vendor.vendor_id', '=', 'tmaster_vendor.id')
                    ->select($this->table.'.*', 'tmaster_vendor.vendor_name')
                    ->where($this->table.'.tpv_id', '=', $id)
                    // ->orderBy('prices_bid', 'asc')
                    ->orderBy($this->table.'.created_at', 'desc')
                    ->get();
    }

    function getCurrentBid($id)
    {
        $data = DB::table($this->table)
                    ->where('tpv_id', '=', $id)
                    // ->orderBy('prices_bid', 'asc')
                    ->orderBy('created_at', 'desc')
                    ->first();

        return $data; 
    }

    function revisePricesBid($post, $id_rev, $idComma)
    {
        $sql = "UPDATE {$this->table} SET prices_bid = '{$post['prices_bid']}', prices_comp = '{$post['prices_comp']}'
                WHERE id = '{$id_rev}'";
        DB::UPDATE($sql);

        $project = $this->getWhereId($id_rev);
        $getMinPrice = $this->getWherePriceMinAndCreated($idComma, $project);
        $this->updateWhereMatch($idComma, $getMinPrice);
        $this->updateWhereNotMatch($idComma, $getMinPrice);
        $this->updateRank($idComma, $project);
    }

    function getWhereId($id)
    {
        return DB::table($this->table)
                ->join('tproject_vendor', $this->table.'.tpv_id', '=', 'tproject_vendor.id')
                ->join('tmaster_project', 'tmaster_project.id', '=', 'tproject_vendor.project_id')
                ->select('tmaster_project.*')
                ->where($this->table.'.id', '=', $id)
                ->get();
    }

    function updateWhereNotMatch($idComma, $price)
    {
        if (!empty($price)) {
            // Update Status sesuai dengan harga yang tidak sesuai
            $sql = "UPDATE {$this->table} SET status = '0', is_active = '0'
                    WHERE tpv_id IN ({$idComma})
                    AND (prices_bid = '{$price[0]->min_price}'
                    AND created_at <> '{$price[0]->created_at}'
                    OR prices_bid <> '{$price[0]->min_price}');";
        
            DB::UPDATE($sql);

            $ids = explode(",", $idComma);
            foreach ($ids as $key => $value) {
                $sql = "SELECT * FROM {$this->table}
                        WHERE tpv_id = {$value} 
                        ORDER BY created_at DESC LIMIT 1;";
                $data = DB::SELECT($sql);
                
                if (!empty($data)) {
                    $sql = "UPDATE {$this->table} SET is_active = '1' WHERE id = '{$data[0]->id}';";
                    DB::UPDATE($sql);
                }
            }
        } else {
            $sql = "UPDATE {$this->table} SET status = '0'
                    WHERE tpv_id IN ({$idComma});";
        
            DB::UPDATE($sql);            
        }
    }

    function updateWhereMatch($idComma, $price)
    {
        if (!empty($price)) {
            // Update Status sesuai dengan harga sesuai
            $sql = "UPDATE {$this->table} SET status = '1'
                    WHERE tpv_id IN ({$idComma})
                    AND (prices_bid = '{$price[0]->min_price}'
                    AND created_at = '{$price[0]->created_at}');";

            DB::UPDATE($sql);
        }
    }

    function getWherePriceMinAndCreated($idComma, $project)
    {
        $low_limit = $project[0]->price_lower_limit;
        $up_limit = $project[0]->price_upper_limit;

        // select harga terendah dari table berdasarkan idComma
        $sql = "SELECT id, tpv_id, prices_bid min_price, is_active, created_at FROM {$this->table}
                WHERE tpv_id IN ({$idComma})  AND is_active = '1'
                AND prices_bid BETWEEN {$low_limit} AND {$up_limit}
                ORDER BY prices_bid, created_at ASC LIMIT 1;";

        return DB::SELECT($sql);
    }

    function getLastPricePerVendor($tpv_id)
    {
        $sql = "SELECT * FROM {$this->table}
                WHERE tpv_id = '{$tpv_id}'
                ORDER BY created_at DESC LIMIT 1;";

        return DB::SELECT($sql);
    }

    function checkLastPrice($data)
    {
        $status = NULL;
        $tpv_id = $data['tpv_id'];

        $sql = "SELECT * FROM {$this->table}
                WHERE tpv_id = '{$tpv_id}'
                ORDER BY created_at DESC LIMIT 1";
        $getLast = DB::SELECT($sql);

        if ($data['prices_bid'] >= $getLast[0]->prices_bid) {
            $status = true;
        } elseif ($data['prices_bid'] <= $getLast[0]->prices_bid) {
            $status = false;
        }
        
        return $status;
    }

    function getBestPrice($project_id)
    {
        return DB::table($this->table)
                    ->join('tproject_vendor', $this->table.'.tpv_id', '=', 'tproject_vendor.id')
                    ->join('tmaster_vendor', 'tproject_vendor.vendor_id', '=', 'tmaster_vendor.id')
                    ->where([
                        [$this->table.'.status', '=', 1],
                        ['tproject_vendor.project_id', '=', $project_id],
                        ])
                    ->select('tmaster_vendor.vendor_name', $this->table.'.prices_bid')
                    ->get();
    }

    function checkRowData($tpv_id)
    {
        return DB::table($this->table)
                    ->where('tpv_id', '=', $tpv_id)
                    ->get();
    }

    function updateRank($idComma, $project)
    {
        // Set all rank to be NULL
        $sql = "UPDATE {$this->table} SET rank = NULL WHERE tpv_id IN ($idComma);";

        DB::UPDATE($sql);

        $ids = explode(",", $idComma);
        $i = 0;
        $arrLastBidVendor = [];

        // id yang harganya diantara batas bawah dan atas
        $idCommas = "";
        // 
        foreach ($ids as $key => $id) {
            $sql = "SELECT a.id, a.tpv_id, a.prices_bid price FROM {$this->table} a
                WHERE a.tpv_id = {$id}
                ORDER BY a.created_at DESC LIMIT 1;";
            $data = DB::SELECT($sql);
            if (!empty($data[0])) {
                if ($data[0]->price < $project[0]->price_lower_limit) {
                    DB::table($this->table)
                        ->where([
                            ['id', '=', $data[0]->id]
                            ])
                        ->update(['rank' => count($ids)]);
                } else {
                    $arrLastBidVendor[$key] = $data[0];
                    $idCommas .= ($idCommas != '') ? ',' : '';
                    $idCommas .= $id;
                }
            }
        }
        
        if ($idCommas != "") {
            // All data yang diantara batas bawah dan batas atas
            $count = count(explode(",", $idCommas));
            $sql = "SELECT * FROM {$this->table}
                    WHERE tpv_id IN ({$idCommas})
                    AND prices_bid BETWEEN {$project[0]->price_lower_limit} AND {$project[0]->price_upper_limit} AND is_active = 1
                    ORDER BY prices_bid, created_at LIMIT {$count};";
            $data = DB::SELECT($sql);
            foreach ($data as $key => $value) {

                DB::table($this->table)
                    ->where([
                        ['id', '=', $value->id]
                        ])
                    ->update(['rank' => ++$key]);
            }
            // End
        }

        // usort($arrLastBidVendor, function($a, $b) { return strcmp($a->price, $b->price); });

        // foreach ($arrLastBidVendor as $key => $value) {
        //         DB::table($this->table)
        //             ->where([
        //                 ['id', '=', $value->id]
        //                 ])
        //             ->update(['rank' => ++$key]);
        // }
    }

    function isAboveMinPrice($tpv_id, $project_min_price)
    {
        $status = false;

        $data = $this->getLastPricePerVendor($tpv_id);

        if ($data[0]->prices_bid >= $project_min_price) {
            $status = true;
        }

        return $status;
    }

    function getBidRanking($idComma)
    {
        $sql = "SELECT a.*, c.id, c.vendor_name, c.vendor_email FROM {$this->table} a
                JOIN tproject_vendor b ON a.tpv_id = b.id
                JOIN tmaster_vendor c ON b.vendor_id = c.id
                WHERE a.tpv_id IN ({$idComma})
                AND a.rank IS NOT NULL
                ORDER BY a.rank ASC;";
        $data = DB::SELECT($sql);

        return $data;
    }
}
