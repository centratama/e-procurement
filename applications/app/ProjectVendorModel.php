<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProjectVendorModel extends Model
{
    //
    function __construct()
    {
    	$this->table = 'tproject_vendor';
    }

    function isAlreadyJoin($project_id, $vendor_id)
    {
    	$status = FALSE;

    	$data = DB::table($this->table)
    				->select('*')
    				->where([
    					['project_id', '=', $project_id],
    					['vendor_id', '=', $vendor_id],
    					])
    				->get();

    	dd($data);
    }

    function insertData($data)
    {
    	return DB::table($this->table)->insertGetId($data);
    }

    function updateStatus($data, $where)
    {
    	return DB::table($this->table)
            		->where($where)
            		->update($data);
    }

    function getWhereIdForVendor($project_id, $vendor_id)
    {
        $data = DB::table($this->table)
                    ->select('*')
                    ->where([
                        ['project_id', '=', $project_id],
                        ['vendor_id', '=', $vendor_id],
                        ])
                    ->get();

        return $data;   
    }

    function getAllIdByProject($id)
    {
        return DB::table($this->table)->where('project_id', $id)->get();
    }

    function getWhereProjectId($project_id)
    {
        return DB::table($this->table)
                    ->join('tmaster_vendor', 'tmaster_vendor.id', '=', $this->table.'.vendor_id')
                    ->select($this->table.'.id as tpv_id', 'tmaster_vendor.*', $this->table.'.*')
                    ->where($this->table.'.project_id', '=', $project_id)
                    ->get();
    }

    function getProjectIdByTpvId($tpv_id)
    {
        return DB::table($this->table)
                    ->join('tmaster_project', 'tmaster_project.id', '=', $this->table.'.project_id')
                    ->select($this->table.'.id as tpv_id', 'tmaster_project.*')
                    ->where($this->table.'.id', '=', $tpv_id)
                    ->get();
    }

    function postWinner($tpv_id, $project_id)
    {
        $sql = "UPDATE {$this->table} SET is_winner = 1
                WHERE id = '{$tpv_id}'
                AND project_id = '{$project_id}';";

        DB::UPDATE($sql);

        $this->postLooser($tpv_id, $project_id);

        return true;
    }

    function postLooser($tpv_id, $project_id)
    {
        $sql = "UPDATE {$this->table} SET is_winner = 0
                WHERE id <> '{$tpv_id}'
                AND project_id = '{$project_id}';";

        DB::UPDATE($sql);        

        return true;
    }

    function getWinner($project_id)
    {
        return DB::table($this->table)
                    ->join('tmaster_vendor', 'tmaster_vendor.id', '=', $this->table.'.vendor_id')
                    ->join('tpv_biding', 'tpv_biding.tpv_id', '=', $this->table.'.id')
                    ->select($this->table.'.id as tpv_id', 'tmaster_vendor.*', 'tpv_biding.*')
                    ->where([
                        [$this->table.'.project_id', '=', $project_id],
                        [$this->table.'.is_winner', '=', 1]
                        ])
                    ->orderBy('tpv_biding.created_at', 'desc')
                    ->limit(1)->get();
    }

    function getById($id)
    {
        $data = DB::table($this->table)->where('id', $id)->get();

        return $data;
    }

    function updateData($post, $id)
    {
        return DB::table($this->table)
                    ->where('id', $id)
                    ->update($post);
    }

    function getWhereProjectIdAndAccept($project_id)
    {
        return DB::table($this->table)
                ->join('tmaster_vendor', 'tmaster_vendor.id', '=', $this->table.'.vendor_id')
                ->select($this->table.'.id as tpv_id', 'tmaster_vendor.*', $this->table.'.*')
                ->where([
                    ['project_id', '=', $project_id],
                    ['status', '=', '1']
                    ])
                ->get();
    }

    function updateWinner($tpv_id, $project_id)
    {
        DB::table($this->table)
            ->where('id', $tpv_id)
            ->update([
                    'is_winner' => 1
                ]);

        DB::table($this->table)
            ->where([
                ['id', '<>',$tpv_id],
                ['project_id', '=', $project_id]
                ])
            ->update([
                    'is_winner' => 0
                ]);        
    }

    function rejectVendorByDueDate($project_id)
    {
        DB::table($this->table)
            ->where([
                ['project_id', '=', $project_id],
                ['invitation_due_date', '<', date("Y-m-d h:i:s")],
                ])
            ->update([
                    'status' => 2
                ]);
    }

    function getRankingByProject($project_id)
    {
        return DB::table($this->table)
                ->join('tmaster_vendor', 'tmaster_vendor.id', '=', $this->table.'.vendor_id')
                ->join('tpv_biding', $this->table.'.id', '=', 'tpv_biding.tpv_id')
                ->select($this->table.'.id as tpv_id', 'tmaster_vendor.*', $this->table.'.*', 'tpv_biding.*')
                ->where([
                    ['project_id', '=', $project_id],
                    [$this->table.'.status', '=', '1'],
                    ['tpv_biding.is_active', '=', '1'],
                    ])
                ->orderBy('rank', 'asc')
                ->get();
    }
}
