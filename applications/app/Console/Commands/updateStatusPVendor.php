<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ProjectVendorController;

class updateStatusPVendor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:statusVendor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Status Project Vendor When Invitation Due Date is Less Than Today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        ProjectVendorController::rejectAllVendorByDueDate();
    }
}
