<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProjectModel extends Model
{
    //
    function __construct()
    {
    	$this->table = 'tmaster_project';
    }

    function getAllData()
    {
    	$data = DB::table($this->table)
                    // ->join('tmaster_site', $this->table.'.site_id', '=', 'tmaster_site.site_id')
    				->join('tmaster_company', $this->table.'.company_id', '=', 'tmaster_company.id')
    				->select($this->table.'.*', 'tmaster_company.company_name')
                    ->orderBy($this->table.'.status', 'asc')
    				->get();

    	return $data;
    }

    function insertData($post)
    {
        return DB::table($this->table)->insertGetId($post);
    }

    function getWhereId($id)
    {
    	$data = DB::table($this->table)
    				->join('tmaster_company', $this->table.'.company_id', '=', 'tmaster_company.id')
    				->select($this->table.'.*', 'tmaster_company.company_name')
    				->where($this->table.'.id', '=', $id)
    				->get();

    	return $data;
    }

    function getProjectByVendorCategory($user_vendor, $catid)
    {
        $sql = "SELECT {$this->table}.*, tmaster_company.company_name 
                FROM {$this->table}
                JOIN tmaster_company ON {$this->table}.company_id = tmaster_company.id
                WHERE {$this->table}.status >= '0'
                AND CONCAT(',', {$this->table}.category_id, ',') REGEXP ',({$catid}),'
                ORDER BY {$this->table}.status ASC;";
        $data = DB::SELECT($sql);
        // $data = DB::table($this->table)
        //             ->join('tmaster_site', $this->table.'.site_id', '=', 'tmaster_site.site_id')
        //             ->join('tmaster_company', 'tmaster_site.company_id', '=', 'tmaster_company.id')
        //             ->join('tmaster_category', $this->table.'.category_id', '=', 'tmaster_category.id')
        //             ->join('tmaster_vendor', 'tmaster_vendor.category_id', '=', 'tmaster_category.id')
        //             ->select($this->table.'.*', 'tmaster_company.company_name', 'tmaster_site.site_id', 'tmaster_site.site_name', 'tmaster_category.category_name')
        //             ->where([
        //                 ['tmaster_vendor.id', '=', $user_vendor],
        //                 [$this->table.'.status', '>=', '1'],
        //                 ])
        //             ->get();

        return $data;
    }

    function publishProject($project_id)
    {
        return DB::table($this->table)
                    ->where('id', $project_id)
                    ->update(['status' => 0]);
    }

    function getWhereIdForVendor($id, $user_vendor)
    {
        $data = DB::table($this->table)
                    ->join('tmaster_site', $this->table.'.site_id', '=', 'tmaster_site.site_id')
                    ->join('tmaster_company', 'tmaster_site.company_id', '=', 'tmaster_company.id')
                    ->join('tmaster_category', $this->table.'.category_id', '=', 'tmaster_category.id')
                    ->join('tmaster_vendor', 'tmaster_vendor.category_id', '=', 'tmaster_category.id')
                    ->join('tproject_vendor', 'tproject_vendor.project_id', '=', $this->table.'.id')
                    ->join('tmaster_vendor', 'tmaster_vendor.id', '=', 'tproject_vendor.vendor_id')
                    ->select($this->table.'.*', 'tmaster_company.company_name', 'tmaster_site.site_id', 'tmaster_site.site_name', 'tmaster_category.category_name', 'tproject_vendor.is_winner', 'tproject_vendor.status as status_pv', 'tproject_vendor.invitation_due_date', 'tproject_vendor.vendor_id')
                    ->where([
                            [$this->table.'.id', '=', $id],
                            ['tmaster_vendor.id', '=', $user_vendor]
                        ])
                    ->get();

        return $data;
    }

    function updateData($post, $project_id)
    {
        return DB::table($this->table)
                    ->where('id', $project_id)
                    ->update($post);
    }

    function getAllDataBOD()
    {
        $data = DB::table($this->table)
                    // ->join('tmaster_site', $this->table.'.site_id', '=', 'tmaster_site.site_id')
                    ->join('tmaster_company', $this->table.'.company_id', '=', 'tmaster_company.id')
                    ->select($this->table.'.*', 'tmaster_company.company_name')
                    ->whereNotNull('status_review')
                    ->orderBy($this->table.'.status', 'asc')
                    ->get();

        return $data;
    }

    function getAllProjectType()
    {
        $data = DB::table('tmaster_project_type')
                ->join('sys_lookup_values', function($join) {
                    $join->on('tmaster_project_type.validation_type', '=', 'sys_lookup_values.lookup_value')
                        ->where('sys_lookup_values.lookup_type', '=','VALIDATION_TYPE');
                })
                ->select('tmaster_project_type.*', 'sys_lookup_values.lookup_value', 'sys_lookup_values.lookup_desc')
                ->get();

        return $data;
    }

    function getProjectTypeById($id)
    {
        $data = DB::table('tmaster_project_type')
                ->join('sys_lookup_values', function($join) {
                    $join->on('tmaster_project_type.validation_type', '=', 'sys_lookup_values.lookup_value')
                        ->where('sys_lookup_values.lookup_type', '=','VALIDATION_TYPE');
                })
                ->select('tmaster_project_type.*', 'sys_lookup_values.lookup_value', 'sys_lookup_values.lookup_desc')
                ->where('tmaster_project_type.id', $id)
                ->first();

        return $data;
    }
}
