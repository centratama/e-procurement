<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CategoryModel extends Model
{
    //
    function __construct()
    {
    	$this->table = "tmaster_category";
    }

    function insertData($post)
    {
    	return DB::table($this->table)->insertGetId($post);
    }

    function getAllData()
    {
    	return DB::table($this->table)->select('*')->get();
    }

    function getWhereId($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id ='{$id}'";
        $data = DB::SELECT($sql);

        return $data;
    }

    function updateData($post, $id)
    {
        return DB::table($this->table)
                    ->where('id', $id)
                    ->update($post);   
    }
}
