<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SysLookupValuesModel extends Model
{
	function __construct()
	{
		$this->table = "sys_lookup_values";
	}
    //
    function getLookupByType($string)
    {
    	$sql = "SELECT * FROM {$this->table}
    			WHERE lookup_type = '{$string}';";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    //
    function getLookupByTypeValue($string, $value)
    {
        $sql = "SELECT * FROM {$this->table}
                WHERE lookup_type = '{$string}'
                AND lookup_value = '{$value}';";

        $data = DB::SELECT($sql);

        return $data;
    }
}
