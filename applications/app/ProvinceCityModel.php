<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProvinceCityModel extends Model
{
    function __construct()
	{
		$this->tableProvince = "tmaster_province";
		$this->tableCity = "tmaster_city";
	}
    //
    function getAllProvince()
    {
    	$sql = "SELECT * FROM {$this->tableProvince};";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    function getAllCity()
    {
    	$sql = "SELECT * FROM {$this->tableCity};";

    	$data = DB::SELECT($sql);

    	return $data;
    }

    function getCityByProvince($province_id)
    {
    	$data = DB::table($this->tableCity)
				->where('province_id', $province_id)
				->orderBy('id', 'asc')
				->lists('city_name','id');

    	return $data;	
    }
}
