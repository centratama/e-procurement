<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UsersModel extends Model
{
    //
    function __construct()
    {
        $this->table = "users";
    }

    public function getData()
    {
    	$sql = "SELECT * FROM {$this->table}";
    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function getWhereId($id)
    {
    	$sql = "SELECT * FROM {$this->table} WHERE id ='{$id}'";
    	$data = DB::SELECT($sql);

    	return $data;
    }

    public function createUser($data)
    {
        DB::table($this->table)->insert($data);
        
        return true; 
    }

    public function activateAcc($user_id, $pass)
    {
        $password = bcrypt($pass);

        $sql = "UPDATE {$this->table}
                SET password ='{$password}', is_active = '1'
                WHERE user_id = '{$user_id}';";

        DB::UPDATE($sql);

        return true;
    }

    public function getWhereByUserId($user_id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE user_id ='{$user_id}'";
        $data = DB::SELECT($sql);

        return $data;
    }

    function updateWhereId($post, $id)
    {
        return DB::table($this->table)
                    ->where('id', $id)
                    ->update($post);
    }
}
